/* blockcache.c */


/* 
 * El cache de bloques se encarga de interactuar entre los pedidos de los diferentes
 * filesystems y los drivers determinados, siendo su principal funci�n la de mantener
 * un cache con los bloques m�s accedidos.
 * Presenta una lista de bloques libres y un hash accedido por device y sector l�gico
 * que conduce a listas de bloques (sus headers m�s precisamente).
 * Existe otra lista de bloques libres (sin uso y utilizados pero de menor a mayor
 * tiempo de acceso LRU) donde puede haber bloques de listas pertenecientes al hash.
 *
 */

#include <routix/device.h>
#include <routix/kalloc.h>
#include <routix/kstdio.h>
#include <routix/task.h>
#include <routix/system.h>
#include <fs/blockcache.h>
#include <sys/list.h>
#include <routix/atomic.h>

#define RUNSTATS 1


#define PAGE_SIZE	 4096

#define MAXHASHENTRIES			10
#define HASH(maxhash,dev,sector)	( (dev+sector)%maxhash )
#define HASH_DEVICE(dev)					(dev)

#define MAKE_MULTIPLE(original,multiple) ((original)+((multiple)-(original)%(multiple)))

// Creamos los headers para manejar las listas de bloques libres y de hash.
static LIST_NEW(block_cache_t)	free_list;
static LIST_NEW(block_cache_t)	cached_list[MAXHASHENTRIES];
static LIST_NEW(block_cache_t)	drv_req_list[MAXDEVICEENTRIES];


// Esta funci�n se encarga de prealocar los bloques y sus respectivos headers
// de acuerdo a la cantidad de memoria total del sistema, utiliza la definici�n
// de MAX_BLOCKCACHEMEM_PCT en system.h (dado en un valor porcentual).
// NOTA: por ahora esta definici�n la dejo ac�.
//
// M�ximo uso de memoria por parte del cache de bloques: 5%
#define MAX_BLOCKCACHE_MEM 5


// Funciones locales
static inline byte *alloc_block();
static inline block_cache_t *alloc_header();
static inline block_cache_t *getfreeblock(void);

// Funciones locales de debug
void show_cached_list(void);


void start_block_cache(int totalmemory)
{
	int blocks;
	int tmp;
	//int pagesforblocks,pagesforheaders;
	

	// Inicializamos los headers de las listas free_list, cached_list y drv_req (drivers request)
	LIST_INIT(free_list);

	for (tmp=0; tmp<MAXHASHENTRIES; tmp++) {
		LIST_INIT(cached_list[tmp]);
	}

	for (tmp=0; tmp<MAXDEVICEENTRIES; tmp++) {
		LIST_INIT(drv_req_list[tmp]);
	}
	
	// Transformamos la cantidad de memoria de Mb a bytes
	totalmemory *= 1024*1024;

	// Calcalumos la cantidad de bloques (redondeando hacia abajo),
	// pero teniendo en cuenta que un bloque posee 512 bytes entrar�n
	// 8 bloques por p�gina, por lo tanto redondemos la cantidad de
	// blocks de modo que sea m�ltiplo de 8
	blocks=totalmemory*MAX_BLOCKCACHE_MEM/(100*BLOCK_SIZE);
	blocks=MAKE_MULTIPLE(blocks,PAGE_SIZE/BLOCK_SIZE);

	// Un bloque es de 512bytes, por lo tanto en una p�gina entran 8 bloques
	//pagesforblocks=blocks/8;

	// Oks, ahora veamos, la cantidad de headers es igual a la cantidad de bloques,
	// pero su tama�o es de sizeof(buffer_block_t), o sea que necesitaremos:
	// memoria_headers=blocks*sizeof(buffer_block_t)
	// y en p�ginas de 4KB:
	// memoria_headers_pag=roundup(memoria_headers/4096)
	// y redondeamos para arriba
	//pagesforheaders=blocks*sizeof(buffer_block_t)/4096 + ( (blocks*sizeof(buffer_block_t))%4096 ) ? 1 : 0;
	kprintf("Inicializando Cache de Bloques con %d bloques\n",blocks);
	byte *datablock;
	block_cache_t *headerblock;
	
	while ( blocks-- ) {

		// Alocamos un bloque de datos
		datablock=alloc_block();

		// su correspondiente header
		headerblock=alloc_header();

		// los vinculamos
		BLOCK_CACHE_BUFFER(headerblock)=datablock;
		BLOCK_CACHE_STATE(headerblock)=UNLOCKED;
		BLOCK_CACHE_LOCKCOUNT(headerblock)=0;
		LIST_CLEAN(BLOCK_CACHE_PENDING_PROCESS_LIST(headerblock));


		// y lo agregamos a la lista de bloques libres
		LIST_ADD(free_list,free,headerblock);

	}
	
}


inline static byte *alloc_block()
{
	static byte *actual_page=NULL;
	static unsigned short int used_blocks=0;

	if ( (actual_page==NULL) || (used_blocks == (PAGE_SIZE/BLOCK_SIZE)) ) {
		actual_page=(byte *) kmalloc_page();
		used_blocks=0;
	}

	return( (byte *) (actual_page+BLOCK_SIZE*(used_blocks++)) );

}

inline static block_cache_t *alloc_header()
{
	static block_cache_t *actual_page=NULL;
	static unsigned short int used_headers=0;

	if ( (actual_page==NULL) || (used_headers >= (PAGE_SIZE/sizeof(block_cache_t))) ) {
		actual_page=(block_cache_t *) kmalloc_page();
		used_headers=0;
	}

	used_headers++;

	return(actual_page++);
}

// Oks, le devolvemos el primer request
inline block_cache_t *getrequest(device_t device)
{

	// El primer request en la lista
	return( LIST_FIRST(drv_req_list[HASH_DEVICE(device)]) );
}

// El floppy nos devolvi� el control, ahora debemos analizar su respuesta
inline void endrequest(block_cache_t *block)
{
	// Lo eliminamos de la lista de request
	LIST_DEL(drv_req_list[HASH_DEVICE( BLOCK_CACHE_DEVICE(block) )],driver_request_list,block);

	// despertamos al proceso que gener� la solicitud (es el primero en la lista)
	despertar_task( LIST_FIRST(BLOCK_CACHE_PENDING_PROCESS_LIST(block)) );
			
}

/*!
 * \brief es la encargada de insertar la solicitud al driver correspondiente
 * \param el bloque solicitado (de tipo block_cache_t)
 */
inline void sendrequest(block_cache_t *block)
{
	// Agregamos el bloque a la lista de solicitudes correspondiente
	// al dispositivo
	LIST_ADD_TAIL(drv_req_list[ HASH_DEVICE( BLOCK_CACHE_DEVICE(block) ) ],driver_request_list,block);

	// Activa el flag que indica tarea para un determinado driver, por ahora no hace nada
	
}


/*!
 * \brief Es la encargada de realizar una lectura desde el cache de bloques y
 * en caso de no poseer dicha solicitud genera la petici�n al dispositivo correspondiente.
 * \param device	dispositivo sobre el que se desea leer el sector (ver routix/device.h)
 * \param sector	sector l�gico a leer
 * \param start	offset desde donde se desea leer el sector
 * \param bufferdst puntero a donde debe copiarse la informaci�n solicitada por el proceso
 * \param	len	largo a leer
 *
 *	\code
 * 
 * Algorithm:
 *
 * function: cache_read(device,sector,start,bufferdst,len)
 * 
 * restart:
 *
 *		START ATOMIC OPERATION
 *		
 *		find (dev,sector) in cached_list
 *
 *		if found {
 *
 *			if ( WRITE_LOCKED or SYNCRONIZING ) {
 *				put this process in the list of io pending processes
 *				sleep process
 *				
 *				FINISH ATOMIC OPERATION
 *				
 *				reschedule
 *				goto restart
 *			}
 *
 *			increment the lock count for this block
 *
 *			if ( UNLOCKED ) {
 *				mark it READ_LOCKED
 *				quit block from free list
 *			}
 *
 *		}
 *
 *		else {
 *			get a free block (via getfreeblock)
 *			mark it SYNCRONIZING
 *			set it device, sector and operation (IOREAD in this case)
 *			insert it into cached list
 *			insert process into io processes pending list
 *			send request to driver
 *			sleep process
 *
 *			FINISH ATOMIC OPERATION
 *			
 *			reschedule
 *
 *			START ATOMIC OPERATION
 *			
 *			quit this process from io processes pending list
 *
 *			wake up all other process waiting for this block
 *			clean io pending list
 *			
 *
 *			if  request has fail {
 *				remove from cached list
 *				add block to front of free list
 *
 *				FINISH ATOMIC OPERATION
 *				
 *				return ERROR
 *			}
 *
 *			mark it READ_LOCKED
 *			increment the lock count for this block
 *			
 *		}
 *
 *		FINISH ATOMIC OPERATION
 *
 *		copy data to buffer
 *
 *		START ATOMIC OPERATION
 * 
 *		decrement lock count
 *		
 *		if ( lock count is 0 ) {
 *			mark block UNLOCKED
 *			put block in TAIL of the free list (LRU)
 *			wake up all other process waiting for this block (they wait for write on it)
 *			clean io pending list
 *		}
 *
 *		FINISH ATOMIC OPERATION
 *		
 *		return SUCCESS
 *
 *	\endcode
 *
 * 
 */
int cache_read(device_t device, word sector, unsigned int start, char *dstbuffer, unsigned int len)
{

	block_cache_t *tmp;
	task_struct_t *proc_iterator;

	#ifdef RUNSTATS
	long long counter;

	if ( getvar("debugcache") == 1 ) 
		kprintf("cache_read: starting, dev: %d, sector: %d, start: %d buffer: 0x%x len: %d\n",device,sector,start,dstbuffer,len);

	if ( getvar("debugcache") == 2 )
		rdtscl(counter);
	#endif

	restart:

	if ( getvar("debugcache") == 1 )
		kprintf("cache_read: buscando bloque en la lista de cache\n");

	START_ATOMIC_OPERATION;

	#ifdef RUNSTATS
	if ( getvar("debugcache")==1 )
		show_cached_list();
	#endif
	
	// Primero buscamos si ya existe el bloque en la lista del cache
	LIST_FOREACH(tmp,cached_list[ HASH(MAXHASHENTRIES,device,sector) ], cached)
		if ( BLOCK_CACHE_DEVICE(tmp)==device && BLOCK_CACHE_SECTOR(tmp)==sector )
			break;

	// Est� cacheado ?
	if ( tmp ) {

		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: cache hit !\n");
		#endif

		// Esta tomado por alguien m�s
		if ( BLOCK_CACHE_STATE(tmp)==WRITE_LOCKED || BLOCK_CACHE_STATE(tmp)==SYNCRONIZING ) {

			// Colocamos el proceso actual al final de la lista de procesos con IO pendiente (el primer
			// nodo es el proceso original que solicit� la IO)
			LIST_ADD_TAIL(BLOCK_CACHE_PENDING_PROCESS_LIST(tmp),io_pending,actual);


			// Ponemos a dormir la tarea
			dormir_task(actual);

			FINISH_ATOMIC_OPERATION;

			// Rescheduleamos
			_reschedule();

			// Oks, en este punto alguien nos despert�, volvemos a intentar tomar el bloque
			// (odio los goto ! :P pero en este momento es una salida r�pida y permite no
			// oscurecer el c�digo.
			goto restart;
		}

		// Se encuentra disponible
		else {
	
			// Si esta deslockeado lo lockeamos, inicializamos la cuenta de locks a cero
			// y lo quitamos de la lista de bloques free
			if ( BLOCK_CACHE_STATE(tmp)==UNLOCKED ) {
				BLOCK_CACHE_STATE(tmp)=READ_LOCKED;
				LIST_DEL(free_list,free,tmp);
				BLOCK_CACHE_LOCKCOUNT(tmp)=0;
			}
			#ifdef RUNSTATS
			else {
				if ( getvar("debugcache")==3 )
				kprintf("Sector: %d relockeando!\n",sector);
				//show_cached_list();
			}
			#endif

		}

	}

	// No est� cacheado
	else {

		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: cache miss(%d) ",sector);
		#endif


		// Obtenemos un bloque de la lista free
		tmp=getfreeblock();

		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: bloque obtenido de la free list 0x%x\n",tmp);

		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: eliminamos este nuevo bloque de la free list\n");
		#endif

		// lo marcamos en Sincronizaci�n
		BLOCK_CACHE_STATE(tmp)=SYNCRONIZING;

		// Seteamos el dispositivo, sector y operacion
		BLOCK_CACHE_DEVICE(tmp)=device;
		BLOCK_CACHE_SECTOR(tmp)=sector;
		BLOCK_CACHE_OPERATION(tmp)=IOREAD;


		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 ) {
			kprintf("cache_read: lo LOCKEAMOS\n");
	    kprintf("cache_read: entrada numero %d en la cached list\n",HASH(MAXHASHENTRIES,device,sector));
		}
		#endif
		
		// y lo insertamos en la lista de bloques cacheados
		LIST_ADD_TAIL(cached_list[ HASH(MAXHASHENTRIES,device,sector) ],cached,tmp);


		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: y lo insertamos al final de la lista de bloques cacheados\n");
		#endif

		// colocamos el proceso actual al principio de la lista de procesos con IO pendiente
		LIST_ADD(BLOCK_CACHE_PENDING_PROCESS_LIST(tmp),io_pending,actual);

		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 ) {
			kprintf("cache_read: insertamos el proceso actual en la lista de procesos con io pendiente\n");
			kprintf("cache_read: enviamos el request\n");
		}
		#endif

		// Realizamos el request, en este punto, sendrequest pondr� a dormir el
		// proceso actual hasta que se complete la petici�n.
		sendrequest(tmp);

		// Ponemos a dormir la tarea
		dormir_task(actual);

		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: nos dormimos\n");
		#endif

		FINISH_ATOMIC_OPERATION;

		// Rescheduleamos
		_reschedule();

		START_ATOMIC_OPERATION;

		#ifdef RUNSTATS
		if ( getvar("debugcache") == 1 )
			kprintf("cache_read: nos despertamos !\n");
		#endif

		// lo quitamos de la lista de procesos io pending
		LIST_DEL(BLOCK_CACHE_PENDING_PROCESS_LIST(tmp),io_pending,actual);

		// despertar al resto de los procesos en espera de este bloque
		LIST_FOREACH(proc_iterator,BLOCK_CACHE_PENDING_PROCESS_LIST(tmp),io_pending) {
			despertar_task(proc_iterator);
		}

		// vaciar la lista de procesos en estado IO
		LIST_CLEAN(BLOCK_CACHE_PENDING_PROCESS_LIST(tmp));


		// El dispositivo concluy� su trabajo, por lo tanto recuperamos el control en este punto.
		//
		// si fall� debemos:
		//		1. quitarlo de la lista de bloques cacheados
		//		2. colocarlo al ppio de la lista de bloques libres
		//		3. retornar error (-1)
		if ( BLOCK_CACHE_RETURN(tmp) == -1 ) {

			LIST_DEL(cached_list[ HASH(MAXHASHENTRIES,device,sector) ],cached,tmp);

			LIST_ADD(free_list,free,tmp);

			FINISH_ATOMIC_OPERATION;

			return -1;

		}

	}

	// Incrementamos la cantidad de locks en �l
	BLOCK_CACHE_LOCKCOUNT(tmp)++;

	// lo marcamos READ_LOCKED
	BLOCK_CACHE_STATE(tmp)=READ_LOCKED;

	FINISH_ATOMIC_OPERATION;

	char *origin = BLOCK_CACHE_BUFFER(tmp) + start;

	// Copiamos los datos leidos al buffer del proceso
	while ( len-- )
		*dstbuffer++ = *origin++;

	START_ATOMIC_OPERATION;

	// Decrementamos la cantidad de locks en �l
	BLOCK_CACHE_LOCKCOUNT(tmp)--;

	if ( BLOCK_CACHE_LOCKCOUNT(tmp)==0 ) {
		
		// Deslockeamos el bloque
		BLOCK_CACHE_STATE(tmp)=UNLOCKED;

		// Lo colocamos al final de la free list
		LIST_ADD_TAIL(free_list,free,tmp);

		// Despertamos todos los procesos que est�n esperando por el bloque
		// y vaciamos la lista
		LIST_FOREACH(proc_iterator,BLOCK_CACHE_PENDING_PROCESS_LIST(tmp),io_pending)
			despertar_task(proc_iterator);

		LIST_CLEAN(BLOCK_CACHE_PENDING_PROCESS_LIST(tmp));

	}

	#ifdef RUNSTATS
  if ( getvar("debugcache") == 2 ) {
		long long int counterfinal;
		rdtscl(counterfinal);
		kprintf("0x%llx ",counterfinal-counter);
	}
	#endif

	FINISH_ATOMIC_OPERATION;

	return 1;

}


/*!	\brief	Obtiene un bloque libre de la lista de bloques libres, debe tenerse
 *					en consideraci�n si el bloque esta marcado como DIRTY (modificado en
 *					memoria y no en disco), por lo que debe realizarse una solicitud de
 *					escritura al device y buscar el siguiente hasta encontrar uno libre
 *
 *	\code
 * Algorithm:
 *
 * function: getfreeblock()
 *
 *
 *	do {
 *
 *		get first block from free list
 *		quit from free list
 *
 *		if block is marked as DIRTY {
 *			send asynchronous write request (that is manage by kernel)
 *		}
 *
 *	} while ( not found free block )
 *
 *	return free block
 *
 *	\endcode
 *
 *	\todo Agregado de manejo de envio de escritura asincr�nica en caso de obtener un bloque marcado como DIRTY (modificado)
 */
static inline block_cache_t *getfreeblock(void)
{

	byte found=0;
	block_cache_t *tmp;
	
	do {

		// Obtenemos el pri		
		tmp=LIST_FIRST(free_list);

		// y lo quitamos de ella
		LIST_DEL(free_list,free,tmp);

		// Bloque marcado como modificado
		if ( BLOCK_CACHE_STATE(tmp)==DIRTY ) {
			// proximo a agregar, manejo de escritura asincr�nica
		}

		// Bloque disponible
		else
			found=1;
		
	} while ( ! found );

	return tmp;

}


void show_cached_list(void)
{
	int iter;
	block_cache_t *tmp;
	char state;

	int cached_blocks=0;

	for(iter=0; iter<MAXHASHENTRIES; iter++) {

		kprintf("Entry %d: ",iter);

		LIST_FOREACH(tmp,cached_list[iter],cached) {
			cached_blocks++;
			
			switch ( BLOCK_CACHE_STATE(tmp) ) {

				case READ_LOCKED:
													state='R';
													break;

				case WRITE_LOCKED:
													state='W';
													break;
													
				case UNLOCKED:
													state='U';
													break;

				case SYNCRONIZING:
													state='S';
													break;

				default:
													state='?';
													break;
													

			}

			if ( BLOCK_CACHE_STATE(tmp)==READ_LOCKED ) {
				kprintf(" %d(%c%d)", BLOCK_CACHE_SECTOR(tmp),state,BLOCK_CACHE_LOCKCOUNT(tmp));
			}
			else {
				kprintf(" %d(%c)", BLOCK_CACHE_SECTOR(tmp),state);
			}
		}

		kprintf("\n");

	}

	kprintf("%d bloques cacheados\n",cached_blocks);
		
}
