/* thread.c */

#include <routix/system.h>
#include <routix/kalloc.h>
#include <routix/thread.h>
#include <string.h>
#include <sys/list.h>
#include <sys/types.h>
#include <sys/bitmap.h>
#include <routix/task.h>

//! NOTA IMPORTANTE: debido a un mal dise�o, dependencias circulares entre header task.h y thread.h,
//!					 los prototipos de algunas funciones aqui implementadas se declaran en task.h


/* Viernes 11 Marzo 2005 Ayer comenz� con la construcci�n de la syscall clone, la cu�l es la base de la creaci�n 
 * de threads. Me surgi� un peque�o problema de dise�o, realmente muy conceptual. Pensaba que los threads compartan
 * el mismo cr3 para que vean exactamente las mismas p�ginas de memoria (a�n cuando pidan memoria con malloc). El problema
 * es que... tendr�an que usar el mismo stack, cosa que es absurdo. O si no, podr�a tener el stack corrido en la memoria
 * pero en donde ? Es decir, limitar�a l�gicamente el stack del thread "padre". Actualmente el m�ximo stack de un 
 * proceso es de 128MB (es decir, la diferencia entre TASK_DATA y TASK_STACK) Excepto que lo mueva a TASK_TASK + ...
 * no se, 32MB ??? 64MB ? Suena mucho 64MB de stack ?... se nota que uds no programan en java
 *
 * Martes 15 Marzo 2005. Hice una implementaci�n muy rudimentaria donde un thread ten�a un task struct asociado como
 * si fuera un proceso m�s. Simplemente al hacer un init_new_task le pasaba para eip la dir de la funci�n a ejecutar por
 * el thread. El thread principal est� atado al proceso. Existe s�lo un CR3, es decir, es el mismo para todos los
 * threads. Cada thread comparte codigo y datos, pero posee diferente user stack y kernel stack.
 * Luego de algunos ajustes logre hacer andar esa aproximaci�n, la cual se encuentra tagueada como Thread_alfa_15-03-2005
 * en el CVS.
 *
 * Ahora en el task_struct del proceso agregue un header de una lista la cual contiene todos los threads del mismo
 * (con excepci�n del thread principal). La idea es que luego de que el scheduler elige al proceso a ejecutar, se
 * verifica si ese proceso tiene threads, y la prioridad de los mismos y comienza a ejecutar al de mayor prioridad.
 *
 */



/*!	\brief	retorna la direccion de comienzo del proximo thread a crear
 *	\param	task_struct de la tarea
 *	\return	direccion de comienzo del stack de user del thread o NULL en caso contrario
 */
addr_t thread_get_stack_address(task_struct_t *task) {
	// Obtener la primera entrada v�lida
	int index;
	index = BITMAP_GET_FIRST_AVAILABLE(task->thread_bitmap, MAX_THREADS_PER_PROCESS, 0);

	// No hay entries libres
	if (index==-1) {
		kpanic("Se supero la cantidad m�xima de threads permitidos de este proceso\n");
		return 0;
	}
	
	// Cuidado porque cambiar esto significa modificar thread_release_stack
	addr_t	address = (index * THREAD_STACK_SIZE) + TASK_STACK + THREAD_STACK_SIZE;
	
	// Seteo ese bit del bitmap como utilizado
	BITMAP_SET_BIT(task->thread_bitmap, index, 1);
	
	return address;
}


/*!	\brief	libera del bitmap el bit correspondiente a la address 
 *	\param	task_struct de la tarea
 *	\param	address direccion del stack
 *	\return	nada por ahora
 *	\note 	es utilizado cuando se libera un stack de un thread(por ej. cuando el thread finaliza)
 */
int thread_release_stack(task_struct_t *task, addr_t address)
{
	int index = (address - (TASK_STACK + THREAD_STACK_SIZE)) / THREAD_STACK_SIZE;
	
	BITMAP_SET_BIT(task->thread_bitmap, index, 0);
	
	return 0;
}	

