/*!  \addtogroup MemoryManager
	\page alloc.c
	Administración de bloques variables de memoria (malloc y free).
*/

#ifndef __SYSTEM
#include "routix/system.h"
#endif
#include <routix/kstdio.h>
#include "routix/paging.h"
#include <routix/kalloc.h>
#include <routix/debug.h>

//! Cantidad de mallocs realizados desde la última llamada a "clear alloc"
volatile int num_mallocs = 0;
//! Cantidad de frees desde la última llamada a "clear alloc"
volatile int num_frees = 0;

unsigned int umalloc_alloc = 0, umalloc_free = 0;

void __free (void *);

//! Punteros utilizados por el stack de páginas libres
addr_t *_inicio,*_fin,*_sp;


//typedef union header Header;

static Header base;
static Header *freep=NULL;

Header *morecore(void);

void *__malloc (unsigned nbytes)
{
	cli();
    Header *p, *prevp;
    unsigned nunits;
    nunits = (nbytes + sizeof(Header)-1) / sizeof(Header) +1;
    if ((prevp = freep)==NULL)     {
		base.s.ptr = freep = prevp = &base;
		base.s.size = 0;
    }

    for ( p=prevp->s.ptr ;   ;  prevp=p , p=p->s.ptr)     {
        if (p->s.size >= nunits) {
		    if (p->s.size == nunits)
				prevp->s.ptr = p->s.ptr;
		    else	{
				p->s.size -= nunits;
				p += p->s.size;
				p->s.size = nunits;
	    	}
		    freep = prevp;
/*TEMP*/	num_mallocs++;
			sti();
		    return (p+1);
		}
		else if (p==freep) {
	    	if ( (p = morecore()) == NULL ) {
	    		sti();
				return NULL;
			}
		}
    }
	sti();
	return NULL;
}



volatile word morecores=0;

Header *morecore(void)
{
    Header *up;

    up = (Header *) kmalloc_page();
    if (up == NULL)	{
        return NULL;
    }

	morecores++;
    
    up->s.size= PAGINA_SIZE / sizeof(Header);
    __free ( (void *) (up+1));
    return freep;
}



void __free (void *ap)
{
	cli();
    Header *bp, *p;

    bp = (Header *) ap - 1;
    for (p=freep; ! (bp > p && bp < p->s.ptr) ; p=p->s.ptr)
        if ( (p >= p->s.ptr)  &&  (bp > p || bp < p->s.ptr))
		    break;
    if ( (bp + bp->s.size) == p->s.ptr)	{
		bp->s.size += p->s.ptr->s.size;
		bp->s.ptr = p->s.ptr->s.ptr;
    }
    else bp->s.ptr = p->s.ptr;

    if ( (p+p->s.size) == bp)	{
		p->s.size += bp->s.size;
		p->s.ptr = bp->s.ptr;
    }
    else p->s.ptr = bp;

    freep = p;    

	num_frees++;
	sti();
}




