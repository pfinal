/*
 *	Wrapper para el manejo y control de alocamiento din�mico de memoria
 */

#include <routix/kalloc.h>
#include <routix/system.h>


// Array donde se guarda la info de alocamiento y liberacion
static mm_wrapper_t mm_info[MM_WRAPPER_MAX_ENTRIES];

// Nombres
static char *alloc_t_names[] = { "TIMER", "FAT12", "USER PAGE", "TASK SIGNALS", "UNKNOWN" };


void init_mm_wrapper()
{
	int i;

	for(i=0; i<MM_WRAPPER_MAX_ENTRIES; i++) {
		ALLOCS(mm_info[i])=0;
		FREES(mm_info[i])	=0;
	}
		
}

inline void *mm_wrapper_malloc(alloc_t type, unsigned int bytes)
{
	if ( type < MM_WRAPPER_UNKNOWN )
		ALLOCS(mm_info[type])++;
	else
		ALLOCS(mm_info[MM_WRAPPER_UNKNOWN])++;
		
	
	return(__malloc(bytes));
}

inline void mm_wrapper_free(alloc_t type, void *ptr)
{
	if ( type < MM_WRAPPER_UNKNOWN )
		FREES(mm_info[type])++;
	else
		FREES(mm_info[MM_WRAPPER_UNKNOWN])++;

	__free(ptr);
}

void mm_wrapper_dump(alloc_t type)
{
		
	// Por control
	if ( type > MM_WRAPPER_UNKNOWN )
		type=MM_WRAPPER_UNKNOWN;

	kprintf("%s allocs=%d frees=%d\n",alloc_t_names[type],ALLOCS(mm_info[type]), FREES(mm_info[type]));

}

void mm_wrapper_dump_all()
{
	int i,type;
	int total_allocs,total_frees;

	for(i=0,total_allocs=0,total_frees=0; i<MM_WRAPPER_MAX_ENTRIES; i++) {

		type=i;
		
		// Por control
		if ( type > MM_WRAPPER_UNKNOWN )
			type=MM_WRAPPER_UNKNOWN;

		kprintf("%s allocs=%d frees=%d\n",alloc_t_names[type],ALLOCS(mm_info[type]), FREES(mm_info[type]));

		total_allocs+=ALLOCS(mm_info[type]);
		total_frees +=FREES(mm_info[type]);
	}

	kprintf("TOTAL allocs=%d frees=%d\n",total_allocs,total_frees);
	
}
