/*!  \addtogroup MemoryManager
	\page kalloc.c
	Administra la memoria del kernel.
*/


#ifndef __SYSTEM
#include "routix/system.h"
#endif
#include <routix/kstdio.h>
#include <routix/kalloc.h>
#include "routix/task.h"
#include "error.h"
#include "routix/debug.h"
#include "routix/paging.h"

#include "routix/misc.h"


addr_t *_inicio,*_fin,*_sp;


/*
 *
 *
 *	_inicio ->	|---------------|
 *			|		|
 *			|		|
 *			|		|
 *	_sp     ->   	|xxxxxxxxxxxxxxx|
 *			|		|
 *			|		|
 *	_fin	->	|---------------|
 *
 * */


//! \brief Inicializa el stack que almacena las p�ginas libres
int kmalloc_page_start (addr_t *inicio, addr_t *fin)
{
_inicio = inicio;
_fin = fin;
_sp = fin;
return OK;
}

//! \brief Retorna al stack una p�gina que el kernel no utiliza
int kfree_page(addr_t direccion)
{
if ( _sp < _inicio ) { return NULL; }
if (getvar("mmdebug")==1)
	kprintf("KFREE: 0x%x\n", direccion);
*_sp-- = direccion;

return 1;
}


addr_t kmalloc_page()
{
    return get_free_page();
}

/*! \brief Entrega una p�gina de memoria
 *  \return addr_t direcci�n de la p�gina
 */
addr_t get_free_page()
{
		
	addr_t direccion;

//	if ((_fin - _sp) < 20)

	if ( _sp > _fin ) { 
		kpanic("No hay mas memoria disponible\n");
		return NULL; 
	}

	_sp++;
	direccion = *_sp;

	if (getvar("mmdebug")==1)
		kprintf("KMALLOC: _inicio=0x%x _fin=0x%x _sp=0x%x - valor: %x\n",_inicio,_fin,_sp,direccion);

	return direccion;
}


/*! \brief Imprime la ubicaci�n de p�ginas libres
 *  \param Cantidad de p�ginas de las cuales se desea conocer su direcci�n
 *  \note Utilizada para prop�sitos de debug
 */
void print_free_pages(word cant)
{
    word i;
    for(i=0 ; i<cant ; i++)
	kprintf("Direccion: %x\n", *(_sp+i));
}	



/*! Bien, supongamos ahora que determinamos la cantidad de memoria del sistema
 * en funci�n de esa cantidad armamos el �rea de stack necesaria para pushear
 * bloques de 4Kb (memoria f�sica libre)
 */
void inicializacion_kmalloc(int memoria_fisica, int memoria_kernel)
{

int memoria_libre;
int mm_stack_size,paginas;
int pagina,pag;

memoria_libre = memoria_fisica - memoria_kernel / 0x100000;

kprintf("Memoria: \tInstalada %d Mb\tKernel %d Mb\tLibre %d Mb\n",memoria_fisica, memoria_kernel/0x100000,memoria_libre);

paginas = 1024*1024*memoria_libre / 4096;
mm_stack_size = 4 * paginas;

kprintf("El tamanio del stack de kmalloc_page para %d paginas es %d bytes - ",paginas,mm_stack_size);
kprintf("Inicio: 0x%x - Fin: 0x%x\n",memoria_kernel, memoria_kernel + mm_stack_size);


kmalloc_page_start( (dword *) memoria_kernel, (dword *) ( memoria_kernel + mm_stack_size ) );

/* Determinamos la primer pagina de memoria libre */
pagina = memoria_kernel + mm_stack_size;

if ( pagina & 0xfff ) {
	pagina += 0x1000 - ( pagina & 0xfff );
}
else {
	pagina = 0x1000 + pagina;
}


for( pag=((memoria_fisica*1024*1024)&0xfffff000)-4096 ; pag >= pagina ; pag -= 0x1000 ) {
	if ( kfree_page( (dword) pag ) == NULL ) { puts("error\n"); }
}

	
}

//! Retorna la cantidad de po�ginas disponibles
inline dword kmem_free (void)
{
    return  (_fin - _sp);
}	




/*! \brief Pide una pagina, completa la estructura user_page, y la mapea en la direcci�n l�gica
 * \param flags: tipo de pagina
 * \param vdir: direcci�n l�gica donde se ubicar� la p�gina
 * \param cr3: direcci�n del directorio de p�ginas
 */
 
struct user_page * umalloc_page ( word flags, addr_t vdir, addr_t cr3)
{
	addr_t aux = get_free_page();
    if (!aux) {
		kprintf("umalloc_page(): error 1: No free memory\n");
		actual->err_no = ENOMEM;
		return NULL;
    }
	// Deberia mapear segun los flags que recibo
	if (kmapmem( aux , vdir , cr3 , PAGE_PRES|PAGE_USER|PAGE_RW) !=OK) {
		kprintf("umalloc_page: error kmapmem\n");
		return NULL;
	}
	
    //struct user_page *mem = (struct user_page *) malloc (sizeof(struct user_page));
    struct user_page *mem = (struct user_page *) MALLOC(MM_WRAPPER_USERPAGE,sizeof(struct user_page));
// DEBUG
    umalloc_alloc++;
//    
    if (!mem) {
		kprintf("umalloc_page(): error 2: No free memory\n");
		kfree_page (aux);
		actual->err_no = ENOMEM;
		return NULL;
    }
    mem->dir = aux;
    mem->vdir = vdir;
    mem->flags = flags;
    mem->count = 1;
    mem->next = NULL;    
    return mem;    
}

/*! Libera una estructura descriptora de pagina de tarea (solo si no esta siendo utilizada por otro proceso)
 * incluyendo al nodo que contiene su direccion 
 */
struct user_page * ufree_page (struct user_page *aux)
{
	aux->count--;			// Un proceso menos que refiere a esta pagina
	if (aux->count > 0)		// Si alguien sigue utilizandola, me voy
		return aux;

	if (aux->dir)			// Si no, la libero
		kfree_page(aux->dir);

// DEBUG
    umalloc_free++;
//
	//free(aux);				// Libero el nodo
	FREE(MM_WRAPPER_USERPAGE,aux);				// Libero el nodo
	return NULL;
}

