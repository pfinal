/*!  \addtogroup BootStrap
	\page kmain.c
	N�cleo de la inicializaci�n del sistema. Una vez que termina de ejecutarse, el control del sistema lo toma la tarea init
*/

#include "routix/system.h"
#include "routix/paging.h"
#include "routix/segm.h"
#include "routix/debug.h"
#include "routix/8259.h"
#include "routix/8254.h"

#include "routix/task.h"
#include "drivers/fat.h"
#include <routix/kalloc.h>
#include "routix/syscalls.h"
#include <routix/kstdio.h>
#include <fs/blockcache.h>

// Puntero a pagina de wrappers de usuario
addr_t *exit_addr;
void init_MM_base(void);
void init_MM (void);
void init_all_memory (dword memoria);
dword contar_memoria (void);

void inicializarInterrupciones (void);
void start_scheduler (void);
void floppy_cache_init (void);
void entrada_de_inicio (void);
void init_time (void);

void init_var (void);


void kmain (void)
{
	/* Deshabilitamos todas las interrupciones */
	mascarasPics(0xff,0xff);

	init_MM();

	/* Seguimos la ejecuci�n en la direcci�n virtual */
	__asm__("aca: jmp aca+5+0xc0000000");

	/* Acomodamos el stack pointer a su valor virtual
	 * 0x200000    es el TOP FISICO  del STACK
	 * 0xd8000000  es el TOP VIRTUAL del STACK */
	__asm__ __volatile__ ("movl $0x200000, %%eax\n\t" \
			      "subl %%esp, %%eax\n\t"     \
			      "movl %%eax, %%esp\n\t"      \
			      "movl $0xd8000000,%%eax\n\t" \
		       	      "subl %%esp, %%eax\n\t"
			      "movl %%eax, %%esp" : : : "eax" );


	clrscr();
	puts("Inicializando kernel....\n");

	puts("Inicializando Controladores Programables de Interrupciones...\n");
	inicializarPics(0x20,0x28);
		
	puts("Kmalloc Inicializando...\n");
	kprintf("Memoria fisica %d Megabytes\n",memoria);

	inicializacion_kmalloc(memoria, KERNEL_END );
	puts("Kmalloc Inicializacion completa\n");

	/* Mapea linealmente el resto de la memoria fisica (a partir de los 4Mb en adelante) */
	init_all_memory(memoria);


	puts("Inicializando interrupciones\n");
	inicializarInterrupciones();

	puts("Inicializando 8254\n");
	init_8254();

	puts("Inicializando Rejoj\n");
	init_time();
	
	// Inicializamos el wrapper de memoria
	init_mm_wrapper();

	// Inicializaci�n del cache de bloques
	start_block_cache(memoria);

	// Ubicar un directorio de p�ginas para las tareas en modo kernel
	USER_PDT = kmalloc_page();
	copy_page ((void *)USER_PDT, (void *)KERNEL_PDT);
	unsigned long *puntero = (unsigned long *)USER_PDT;
	int i;
	// A los 2 primeros GB de memoria los pongo como Supervisor Only
	for (i=0 ; i<PAGINA_SIZE ; i++)
		puntero[i] = puntero[i] & 0xfffffffb;


	// Inicializaci�n del scheduler
	puts("Inicializando el scheduler\n");
	start_scheduler();

	/*
	extern dev_fat_t dev_fat[1];		//Estructura para dispositivos con fs FAT
    dev_fat[0].boot_leido = FALSE;		
	dev_fat[0].fat_levantada = FALSE;
	floppy_cache_init();
	*/
	
	kprintf("Task Struct Size: %d\n", sizeof(task_struct_t));

	// Habilitamos solo teclado, timertick y floppy
	enable_irq(0);
	enable_irq(1);
	enable_irq(6);

	// Inicializar variables USER-KERNEL (moemntaneas) definidas en sysmisc.c
	init_var();

	entrada_de_inicio();

}

