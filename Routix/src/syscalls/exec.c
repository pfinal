#include <routix/system.h>
#include <routix/paging.h>
#include <routix/debug.h>
#include <routix/syscalls.h>
#include <sys/syscalls.h>
#include <routix/elf.h>
#include <routix/file.h>
#include <error.h>
#include <routix/kalloc.h>
#include <routix/kstdio.h>
#include <string.h>
#include <routix/task.h>
#include <sys/list.h>
#include <sys/types.h>

#define RUNSTATS 1

/*! \brief Ejecuta un nuevo proceso sobre el espacio de direcciones del proceso actual
 *  \param nombre path de la tarea a ejecutar
 *  \param arg_p vector de punteros a argumentos de main
 *  \param env_p vector de punteros a variables de entorno exportadas por el shell
 *  \return 0 y -1 en caso de �xito y error, respectivamente. Se setea el valor de err_no del task_struct
 *  \todo Liberar recursos en caso de error
 *  \todo Reemplazar todas las llamadas de exec por execve
 */
int sys_execve (char *nombre, char **arg_p, char **env_p)
{
    nombre = convertir_direccion( nombre , actual->cr3_backup);
 
    int fd;
    word i;
    
    int bytes_leidos;
    
    fd = open(nombre);
    if (fd < 0) {
		actual->err_no = ENOENT;
        return -1;
    }
    
    struct exec_file coff;
	// Momentaneamente solo se puede ejecutar un COFF
    if (read_coff_header(fd, &coff)==-1) {
    	kpanic("read_coff_header return");
    }
    
	unsigned int paginas_texto, paginas_data, paginas_datos;
	
	paginas_texto = coff.code_pages;
	paginas_data = coff.data_pages;
	paginas_datos = coff.alldata_pages;

    task_struct_t *new_task;
    struct user_page *mem;
    
    // Poner el nombre de archivo como descripcion
    char nuevo_nombre[13];
    tomar_nombre_tarea(nombre, nuevo_nombre);

    // init_new_task devuelve la tarea creada como TASK_STOPPED, la mantendremos as� hasta el final    
    new_task = init_new_task(DESC_CODE_USUARIO, DESC_DATA_USUARIO, TASK_TEXT, TASK_STACK - 16, 0x202,\
		    nuevo_nombre, START_PRIORITY);
	
    if (!new_task) {	    //liberar
		return -1;
    }

	new_task->num_code = paginas_texto;
    new_task->num_data = paginas_datos;
    
	if ( getvar("stack")==-1)    
    	new_task->num_stack = 1;
    else new_task->num_stack = getvar("stack");
//    new_task->num_stack = TASK_STACK_SIZE;		// Esto momentaneamente es una variable (definida en sched.c)

	
    new_task->mstack = umalloc_page (PAGINA_STACK, TASK_STACK - PAGINA_SIZE, new_task->cr3);
    if (!new_task->mstack)  //liberar
		return -1;
    mem = new_task->mstack;

    for( i=1 ;  i < new_task->num_stack ; i++) {
		mem->next = umalloc_page (PAGINA_CODE, TASK_STACK  - ((i+1)*PAGINA_SIZE), new_task->cr3);
		mem = mem->next;
	}
    

    
    // Levantar el .TEXT
    lseek(fd, coff.code_off, SEEK_SET);
    new_task->mcode = umalloc_page (PAGINA_CODE,TASK_TEXT, new_task->cr3);
    if (!new_task->mcode)    //liberar
		return -1;
    mem = new_task->mcode;
    bytes_leidos =  read( fd, (void *)mem->dir, PAGINA_SIZE);
	
    for( i=1 ;  i < paginas_texto ; i++) {
		mem->next = umalloc_page (PAGINA_CODE, TASK_TEXT + (i*PAGINA_SIZE) , new_task->cr3);
		mem = mem->next;
        bytes_leidos =  read(fd, (void *)mem->dir , PAGINA_SIZE);
		if (bytes_leidos < 0) {	    //error de algo.... liberar memoria
		    actual->err_no = EIO;
		    close(fd);
		    return -1;
		}
    }

    //Pedir memoria para los datos (.DATA + .BSS)
    new_task->mdata = umalloc_page (PAGINA_DATA, TASK_DATA, new_task->cr3);
    if (!new_task->mdata)   //liberar
		return -1;
    for ( mem=new_task->mdata,i=1 ; i<paginas_datos ; i++, mem=mem->next ) {
        mem->next = umalloc_page (PAGINA_DATA, TASK_DATA + (i*PAGINA_SIZE) , new_task->cr3);
		if (!mem->next)	    //liberar
		    return -1;
    }

    //Levantar .DATA
    lseek(fd, coff.data_off, SEEK_SET);
    for( i=0, mem=new_task->mdata ;   i < paginas_data  ;  i++, mem=mem->next ) {
		bytes_leidos =  read(fd, (void *)mem->dir, PAGINA_SIZE);
		if (bytes_leidos < 0) {	    //error de algo.... liberar memoria
		    actual->err_no = EIO;
		    close(fd);
		    return -1;
		}
    };

	// **** A partir de aqui se ubica al fondo del stack el envp, argv, argc y el retorno del main **** //
	
	// Me ubico en la ultima pagina (la cual usare para exit, argv, etc, etc)
    for( mem=new_task->mdata ;  mem->next  ;  mem=mem->next);
	
	unsigned char *ptr_exit = (unsigned char *) mem->dir;
	
	// Ubicamos en el wrapper el codigo de libreria routstd de llamada EXIT:
	*ptr_exit = 0xb8; 	// Codigo de operacion: "mov eax, "
	*(unsigned long *)(ptr_exit+1) = SYS_PROCESS | SYS_EXIT; 
	*(ptr_exit+5) = 0xbb; // "mov ebx, "
	*(unsigned long *)(ptr_exit+6) = 0; // parametro que recibe la funci�n EXIT... 0 en este caso
	*(ptr_exit+10) = 0xcd;	// int
	*(ptr_exit+11) = 0x50;	// 0x50
	
	// Hardcodeo la llamada al sistema sys_signal_check, la cu�l se ejecutar� luego de cada handler de se�a
	*(ptr_exit+12) = 0xb8; 	// Codigo de operacion: "mov eax, "
	*(unsigned long *)(ptr_exit+13) = SYS_SIGNALS | SYS_SIGNAL_CHECK; 
	*(ptr_exit+17) = 0xcd;	// int
	*(ptr_exit+18) = 0x50;	// 0x50
	*(ptr_exit+19) = 0x5e;	// pop esi	// Con estos popeos recupero los registros de prop�sito general
	*(ptr_exit+20) = 0x5f;	// pop edi  // que fueron pusheados antes de ejecutar un handler de se�al
	*(ptr_exit+21) = 0x5a;	// pop edx
	*(ptr_exit+22) = 0x59;	// pop ecx
	*(ptr_exit+23) = 0x5b;	// pop ebx
	*(ptr_exit+24) = 0x58;	// pop eax
	*(ptr_exit+25) = 0xc3;	// retn
	
	TASK_SIGNALS_ALLOC(new_task);
	memset (TASK_SIGNALS(new_task), 0, sizeof(struct task_signals));
	TASK_SIGADDR(new_task) = GET_OFFSET(ptr_exit + 12) + mem->vdir;
	
	#define EXIT_CALL_LEN		12
	#define SIGNAL_CHECK_LEN	14
	
	// Traspaso de argumentos y variables de entorno
	arg_p = convertir_direccion(arg_p, actual->cr3_backup);
	env_p = convertir_direccion(env_p, actual->cr3_backup);

	char **argv, **envp; 
	// Cantidad de argumentos, m�s el nombre del ejecutable
	int argc = count_elements (arg_p) + 1;
	// Cantidad de variables de entorno seteadas
	int envc = count_elements (env_p);

	char *aux_dest;
	argv = (char **) ptr_exit + EXIT_CALL_LEN + SIGNAL_CHECK_LEN;
	aux_dest = (char *)(argv + argc + 1);			// Empiezo a poner los strings despues del vector de punteros argv
	strcpy(aux_dest, nuevo_nombre);
	int z;

	for (z=0 ; z<argc ; z++) {
		argv[z] = GET_OFFSET(aux_dest) + (char *) mem->vdir;
		aux_dest += strlen(aux_dest) + 1;
		strcpy(aux_dest, convertir_direccion(arg_p[z], actual->cr3_backup));
	}
	// Coloco el NULL como ultimo elemento de vector de punteros
	argv[z+2] = NULL;

	// Alinear a 4 bytes
	envp = (char **) ALINEAR_4(aux_dest + strlen(aux_dest) + 4);

	aux_dest = (char *) (envp + envc + 2);
	for (z=0 ; z<envc ; z++) {
		strcpy(aux_dest, convertir_direccion(env_p[z], actual->cr3_backup));
		envp[z] = GET_OFFSET(aux_dest) + (char *) mem->vdir;
		aux_dest += strlen(aux_dest) + 1;
	}
	// Coloco el NULL como ultimo elemento de vector de punteros
	envp[z+1] = NULL;
	// TEMPORAL (no recuerdo porque le puse temporal !!!)
	argv = (char **)(GET_OFFSET(argv) + (char *) mem->vdir);
	envp = (char **)(GET_OFFSET(envp) + (char *) mem->vdir);
	
	// Mando al fondo del Stack a envp, argv, argc y el RET a sys_exit
	unsigned long *qwe = (unsigned long *) (new_task->mstack->dir + 4096 - 4);
	*(qwe--) = (unsigned long) envp;
	*(qwe--) = (unsigned long) argv;
	*(qwe--) = (unsigned long) argc;
	*qwe = (unsigned long) mem->vdir;


	if (actual==pre_init_task) {				// Si el proceso es init, caso particular ya que init no tiene formato de tarea
	    TASK_PARENT(new_task) = actual;
		new_task->pid = get_new_pid();
		new_task->padre = pre_init_task;
		// A los procesos que "executa init" le inicializo la lista de hijos, ya que no la van a reemplazar
		// a init
		LIST_INIT (new_task->childs);
		// Agregarse como hijo de init
		LIST_ADD (actual->childs, brothers, new_task);
	}
	else {								// Si es cualquier otro proceso, el nuevo proceso debe heredar su pid y ppid
	    TASK_PARENT(new_task) = TASK_PARENT(actual);
	    new_task->pid = actual->pid;
		new_task->padre = actual->padre;
		LIST_DEL(actual->padre->childs, brothers, actual);
		LIST_ADD_TAIL(actual->padre->childs, brothers, new_task);
	}

	
    close(fd);

    // Despertamos la nueva tarea, recordemos que init_new_task crea las tareas en estado TASK_STOPPED
    despertar_task( new_task );

	// Recordemos que el proceso que ejecuto el exec debe terminar (excepto que sea el init)
	if (actual->pid!=1) {
		cli();
		sys_exit_mm(actual);					// libero la memoria
		remover_task (actual);			// lo saco de la lista de tareas del scheduler
		kfree_page ((addr_t)actual);	// libero el task_struct
		sti();
		_reschedule();					
	}
    return OK;
}





/*!	\brief	Lee el header de un COFF32, obteniendo la informaci�n necesaria para ejecutarlo
 *	\param	fd descriptor del archivo (el cu�l debe estar abierto)
 *	\param  coff direcci�n de estructura exec_file donde se colocar� la informaci�n le�da
 *	\return 0 en caso de �xito, -1 en caso contrario
 *
 */
int read_coff_header (int fd, struct exec_file *coff)
{
    struct coff_header *p;
    struct coff_sections sec_text, sec_data, sec_bss;
    
    byte *buff_aux = (byte *) malloc (sizeof(byte) * 200);

	if (!buff_aux)
		return -1;
		

    // Leer cabecera basica del COFF32
    read(fd, buff_aux, sizeof(struct coff_header));
    p = (struct coff_header *) buff_aux;

    if (p->f_magic != COFF32_TYPE) {	    // Verificar numero magico
		actual->err_no = ENOEXEC;
		close(fd);
		free (buff_aux);
		return -1;
    }
    byte num_secciones;
    if ( (num_secciones = p->f_nscns)!= 3) {
		actual->err_no = ENOEXEC;
		close(fd);
		free (buff_aux);
		return -1;
    }
    // Pararse donde comienzan las estructuras de las secciones ( header + datos_opcionales)
    lseek (fd, sizeof(struct coff_header) + p->f_opthdr , SEEK_SET);

    word verificacion = 0;	// Flag usado para verificar la existencia de .TEXT, .DATA y .BSS

    struct coff_sections *q;
    //Levantar las 3 secciones que debe tener el ejecutable
    while (num_secciones--) {
		read(fd, buff_aux, sizeof(struct coff_sections));
		q = (struct coff_sections *) buff_aux;
    
		if ( q->s_flags == COFF32_TEXT ) {
		    memcpy ( &sec_text , q , sizeof(struct coff_sections) );
		    verificacion = verificacion | COFF32_TEXT;
		}
		else if ( q->s_flags == COFF32_DATA ) {
		    memcpy ( &sec_data , q , sizeof(struct coff_sections) );
		    verificacion = verificacion | COFF32_DATA;
		}
		else if ( q->s_flags == COFF32_BSS ) {
		    memcpy ( &sec_bss , q , sizeof(struct coff_sections) );
		    verificacion = verificacion | COFF32_BSS;
		}
		else {	    //No se puede identificar el tipo de seccion
		    close(fd);
		    actual->err_no = ENOEXEC;
			free (buff_aux);
		    return -1;
		}
    }
        	    
    //Verificar que posea una seccion de codigo, una de datos inicializados y sin inicializar
    if (verificacion != (COFF32_TEXT | COFF32_DATA | COFF32_BSS) ) {
    	close(fd);
		actual->err_no = ENOEXEC;
		free (buff_aux);
		return -1;
    }
    
    //Verificar que haya memoria suficiente    
    if (kmem_free() < ((actual->open_files[fd]->size / PAGINA_SIZE)+8) ) {
		actual->err_no = ENOMEM;
		close(fd);
		free (buff_aux);
		return -1;
    }

    // Aca voy a guardar la cantidad de paginas requeridas por cada segmento
    word paginas_texto, paginas_datos;

    paginas_texto = sec_text.s_size / PAGINA_SIZE;
    if ( sec_text.s_size % PAGINA_SIZE ) {
		paginas_texto++;
	}
    //Tama�o en bytes del .DATA + .BSS
    int size_datos = sec_data.s_size + sec_bss.s_size;

    //Cantidad de paginas requeridas por .DATA + .BSS
    paginas_datos = size_datos / PAGINA_SIZE;
    if ( size_datos % PAGINA_SIZE )
		paginas_datos++;

	paginas_datos++;		// Uso una pagina de datos para (argc, **argv, **env) y llamada implicita a EXIT
	
    //Cantidad de paginas requeridas solo por .DATA
    int paginas_data = sec_data.s_size / PAGINA_SIZE;
    if ( sec_data.s_size % PAGINA_SIZE )
		paginas_data++;

	coff->code_off = sec_text.s_scnptr;
	coff->data_off = sec_data.s_scnptr;

	coff->code_pages = paginas_texto;
	coff->data_pages = paginas_data;
	coff->alldata_pages = paginas_datos;
	
	free (buff_aux);
	return 0;
}


