/*!  \addtogroup Syscalls
	\page sys_proc.c
	Contiene las llamadas al sistema relacionadas con procesos.
*/


#include <routix/system.h>
#include <routix/paging.h>
#include <routix/segm.h>
#include <routix/debug.h>
#include <routix/syscalls.h>
#include <sys/syscalls.h>
#include <routix/elf.h> 
#include <routix/file.h>
#include <error.h>
#include <routix/timer.h>
#include <routix/kalloc.h>
#include <routix/kstdio.h>
#include <string.h>
#include <routix/task.h>
#include <sys/list.h>
#include <sys/types.h>
#include <routix/signal.h>
#include <fs/blockcache.h>
#include <sys/bitmap.h>
#include <routix/thread.h>

#define RUNSTATS 1

//! Puntero al task struct de la tarea que realiz� la llamada al sistema
extern task_struct_t *actual;
//! Puntero al task struct del proceso init
extern task_struct_t *init_task;

int sys_execve (char *nombre, char **arg_p, char **env_p);
inline int count_elements (char **vector);

// Funciones relacionadas con la llamada "exit"
//int sys_exit_mm(void);
int sys_exit_notify(void);
int sys_exit_fs (void);


//! Vector de funciones de llamadas al sistema (grupo Process)
int (*syscall_process[MAX_SYSCALLS]) (void) = {
	(int (*) (void)) sys_exec,	
	(int (*) (void)) sys_void,	
	(int (*) (void)) sys_fork,	
	(int (*) (void)) sys_perror,	
	(int (*) (void)) sys_renice,	
	(int (*) (void)) sys_get_pid,
	(int (*) (void)) sys_get_ppid,
	(int (*) (void)) sys_exit,
	(int (*) (void)) sys_show,
	(int (*) (void)) sys_waitpid,
	(int (*) (void)) sys_execve,
	(int (*) (void)) sys_clone
};

   
//! S�lo imprime un mensaje (utilizada tiempo atr�s para prop�sitos de debug)
int sys_void (void)
{
    kprintf("\nLlamada a SYS Void\n");
	unsigned long *_esp;
	__asm__ __volatile__ ("movl %%esp, %0" : "=m" (_esp));
	kprintf("VOIDO: valor de ESP: 0x%x\n",   *(_esp));
	kprintf("VOIDO: valor de ESP+1: 0x%x\n", *(_esp+1));
	kprintf("VOIDO: valor de ESP+2: 0x%x\n", *(_esp+2));
	kprintf("VOIDO: valor de ESP+3: 0x%x\n", *(_esp+3));
	kprintf("VOIDO: valor de ESP+4: 0x%x\n", *(_esp+4));
	kprintf("VOIDO: valor de ESP+5: 0x%x\n", *(_esp+5));
	kprintf("VOIDO: valor de ESP+6: 0x%x\n", *(_esp+6));
	kprintf("VOIDO: valor de ESP+7: 0x%x\n", *(_esp+7));
	kprintf("VOIDO: valor de ESP+8: 0x%x\n", *(_esp+8));
	kprintf("VOIDO: valor de ESP+9: 0x%x\n", *(_esp+9));
	kprintf("VOIDO: valor de ESP+10: 0x%x\n", *(_esp+10));
	kprintf("VOIDO: valor de ESP+11: 0x%x\n", *(_esp+11));
	kprintf("VOIDO: valor de ESP+12: 0x%x\n", *(_esp+12));
	kprintf("VOIDO: valor de ESP+13: 0x%x\n", *(_esp+13));
	kprintf("VOIDO: valor de ESP+14: 0x%x\n", *(_esp+14));
	kprintf("VOIDO: valor de ESP+15: 0x%x\n", *(_esp+15));
	kprintf("VOIDO: valor de ESP+16: 0x%x\n", *(_esp+16));
	kprintf("VOIDO: valor de ESP+17: 0x%x\n", *(_esp+17));

	unsigned long *p = (unsigned long *) *(_esp+9);
	p = convertir_direccion (p, actual->cr3_backup);


	return OK;
}	

//! Estructuras utilizadas por fork()
//! Registros que se pushean al stack de modo kernel al entrar via int 0x50
struct int_regs
{
    dword ebx;
    dword ecx;
    dword edx;
    dword edi;
    dword esi;
    dword ebp;
    dword eip;
    dword cs;
    dword eflags;
    dword esp;
    dword ss;
};


//! Implementaci�n de fork (no hace falta hacer comentarios)
int sys_fork (void)
{
	cli();

    struct task_struct_t *new_task;
    // Punteros al contexto de la tarea (en el stack de modo kernel)
    struct int_regs *new, *current;
    struct int_regs_ext *new_ext;
    word i;

    // init_new_task devuelve la tarea creada como TASK_STOPPED, la mantendremos as� hasta el final    
    new_task = init_new_task (0,0,0,0,0, actual->descripcion, actual->prioridad);
    
    current = (struct int_regs *) ((actual->esp0 & 0xfffff000) + PAGINA_SIZE - sizeof(struct int_regs));
    new = (struct int_regs *) ((new_task->esp0 & 0xfffff000) + PAGINA_SIZE - sizeof(struct int_regs));

    new_ext = (struct int_regs_ext *) ((new_task->esp0 & 0xfffff000) + PAGINA_SIZE - sizeof(struct int_regs_ext));
    new_ext = GET_CONTEXT(new_task);
    
    new_ext->ds = DESC_DATA_USUARIO;
    new_ext->es = DESC_DATA_USUARIO;
    new_ext->fs = DESC_DATA_USUARIO;
    new_ext->gs = DESC_DATA_USUARIO;
    
    new_task->esp0 = (new_task->esp0 & 0xfffff000) + PAGINA_SIZE - 64;
    
    // Copiar los contextos
    memcpy(new, current, sizeof (struct int_regs) );

    // Duplicar los indicadores de cantidad de paginas usadas por los segmentos
    new_task->num_code = actual->num_code;
    new_task->num_data = actual->num_data;
    new_task->num_stack = actual->num_stack;
   
    struct user_page *mem;
    mem = new_task->mcode = actual->mcode;
    
    //Mapear las direcciones del segmento de codigo en memoria virtual, e incrementar el contador de procesos (count)
    for ( i=0 ; i < new_task->num_code ; i++,mem=mem->next ) {
		if ( kmapmem( mem->dir , mem->vdir , new_task->cr3 ,PAGE_PRES|PAGE_USER|PAGE_RW)!=OK ){
		    kprintf("Kmapmem TASK_TEXT error\n");
		    return -1;
		}
		mem->count++;
    }

    struct user_page *src = actual->mdata;
    struct user_page *dest;

    //Para datos y bss debo pedir memoria, copiar la del proceso actual en ella, y finalmente mapearla
    for( i=0  ;  i < new_task->num_data ;  i++) {
		if (i==0)
			dest = umalloc_page ( src->flags, src->vdir, new_task->cr3 );
		else 	{
			dest->next = umalloc_page ( src->flags, src->vdir, new_task->cr3 );
			dest = dest->next;
		}
		if (!dest) { //liberar
		    actual->err_no = ENOMEM;
		    return -1;
		}
		if (i==0)	//Apuntar el mdata al comienzo de la lista
		    new_task->mdata = dest;

		// Copiar el Stack del padre
		copy_page ( (void *)dest->dir, (void *)src->dir);
		src = src->next;
    }

	// Pido lugar para las p�ginas de stack y las copio
    src = actual->mstack;
    for( i=0  ;  i < new_task->num_stack ;  i++) {
		if (i==0)
			dest = umalloc_page ( src->flags, src->vdir, new_task->cr3 );
		else	{
			dest->next = umalloc_page ( src->flags, src->vdir, new_task->cr3 );
			dest = dest->next;
		}
		if (!dest) { //liberar
		    actual->err_no = ENOMEM;
		    return -1;
		}
		if (i==0)	//Apuntar el mdata al comienzo de la lista
		    new_task->mstack = dest;

		copy_page ( (void *)dest->dir, (void *)src->dir);
		src = src->next;
    }

	TASK_SIGNALS_ALLOC(new_task);
	memcpy(TASK_SIGNALS(new_task), TASK_SIGNALS(actual), sizeof(struct task_signals));

    new_task->pid = get_new_pid();
    TASK_PARENT(new_task) = actual;
	new_task->padre = actual;


	// Inicializo el header de la lista de hijos
	LIST_INIT (new_task->childs);
	// Me agrego en la lista de hijos de mi padre
	LIST_ADD_TAIL(actual->childs, brothers, new_task);

	// Inicializo el header de la lista de hijos zombies
//	LIST_INIT (new_task->zombie_header);
	
    // Poner en eax del hijo un 0, para que tome 0 como el retorno de fork
    new_ext->eax = 0;

	// Despertamos la nueva tarea, recordemos que init_new_task crea las tareas en estado TASK_STOPPED
    despertar_task( new_task );
	sti();

    return (new_task->pid);
}


/*! \brief Genera un nuevo proceso a partir de un ejecutable COFF32
 *  \param path donde se encuentra el ejecutable
 *  \return -1 en caso de error.
 *  \todo Liberar recursos en caso de error
 */

int sys_exec (char *nombre)
{
	kprintf("Esta llamada al sistema no se encuentra disponible, por favor, utilizar execve\n");
	return -1;
}
/*
#ifdef RUNSTATS
		long long counter;

		if ( getvar("debugexec") == 2 ) {
			rdtscl(counter);
		}
#endif
	

    nombre = convertir_direccion( nombre , actual->cr3_backup);
 
    int fd;
    byte buff_aux[100];	    //WARNING, agota el stack
    word i;
    
    struct coff_header *p;
    struct coff_sections sec_text, sec_data, sec_bss;

    int bytes_leidos;
    
    fd = open(nombre);
    if (fd < 0) {
		actual->err_no = ENOENT;
        return -1;
    }
    
    // Leer cabecera basica del COFF32
    read(fd, buff_aux, sizeof(struct coff_header));
    p = (struct coff_header *) buff_aux;

    if (p->f_magic != COFF32_TYPE) {	    // Verificar numero magico
		actual->err_no = ENOEXEC;
		close(fd);
		return -1;
    }
    byte num_secciones;
    if ( (num_secciones = p->f_nscns)!= 3) {
		actual->err_no = ENOEXEC;
		close(fd);
		return -1;
    }
    // Pararse donde comienzan las estructuras de las secciones ( header + datos_opcionales)
    lseek (fd, sizeof(struct coff_header) + p->f_opthdr , SEEK_SET);

    word verificacion = 0;	// Flag usado para verificar la existencia de .TEXT, .DATA y .BSS

    struct coff_sections *q;
    //Levantar las 3 secciones que debe tener el ejecutable
    while (num_secciones--) {
		read(fd, buff_aux, sizeof(struct coff_sections));
		q = (struct coff_sections *) buff_aux;
    
		if ( q->s_flags == COFF32_TEXT ) {
		    memcpy ( &sec_text , q , sizeof(struct coff_sections) );
		    verificacion = verificacion | COFF32_TEXT;
		}
		else if ( q->s_flags == COFF32_DATA ) {
		    memcpy ( &sec_data , q , sizeof(struct coff_sections) );
		    verificacion = verificacion | COFF32_DATA;
		}
		else if ( q->s_flags == COFF32_BSS ) {
		    memcpy ( &sec_bss , q , sizeof(struct coff_sections) );
		    verificacion = verificacion | COFF32_BSS;
		}
		else {	    //No se puede identificar el tipo de seccion
		    close(fd);
		    actual->err_no = ENOEXEC;
		    return -1;
		}
    }
        	    
    //Verificar que posea una seccion de codigo, una de datos inicializados y sin inicializar
    if (verificacion != (COFF32_TEXT | COFF32_DATA | COFF32_BSS) ) {
    	close(fd);
		actual->err_no = ENOEXEC;
		return -1;
    }
    
    //Verificar que haya memoria suficiente    
    if (kmem_free() < ((actual->open_files[fd]->size / PAGINA_SIZE)+8) ) {
		actual->err_no = ENOMEM;
		close(fd);
		return -1;
    }

    // Aca voy a guardar la cantidad de paginas requeridas por cada segmento
    word paginas_texto, paginas_datos;

    paginas_texto = sec_text.s_size / PAGINA_SIZE;
    if ( sec_text.s_size % PAGINA_SIZE ) {
		paginas_texto++;
	}
    //Tama�o en bytes del .DATA + .BSS
    int size_datos = sec_data.s_size + sec_bss.s_size;

    //Cantidad de paginas requeridas por .DATA + .BSS
    paginas_datos = size_datos / PAGINA_SIZE;
    if ( size_datos % PAGINA_SIZE )
		paginas_datos++;

	paginas_datos++;		// Uso una pagina de datos para (argc, **argv, **env) y llamada implicita a EXIT
	
    //Cantidad de paginas requeridas solo por .DATA
    int paginas_data = sec_data.s_size / PAGINA_SIZE;
    if ( sec_data.s_size % PAGINA_SIZE )
		paginas_data++;


    task_struct_t *new_task;
    struct user_page *mem;
    
    // Poner el nombre de archivo como descripcion
    char nuevo_nombre[13];
    tomar_nombre_tarea(nombre, nuevo_nombre);

    // init_new_task devuelve la tarea creada como TASK_STOPPED, la mantendremos as� hasta el final    
    new_task = init_new_task(DESC_CODE_USUARIO, DESC_DATA_USUARIO, TASK_TEXT, TASK_STACK + 4096 - 16, 0x202,\
		    nuevo_nombre, START_PRIORITY);
	
    if (!new_task) {	    //liberar
		return -1;
    }

    new_task->mstack = umalloc_page (PAGINA_STACK, TASK_STACK, new_task->cr3);
    if (!new_task->mstack)  //liberar
		return -1;

	new_task->num_code = paginas_texto;
    new_task->num_data = paginas_datos;
    new_task->num_stack = 1;
    
    // Levantar el .TEXT
    lseek(fd, sec_text.s_scnptr,SEEK_SET);
    new_task->mcode = umalloc_page (PAGINA_CODE,TASK_TEXT, new_task->cr3);
    if (!new_task->mcode)    //liberar
		return -1;
    mem = new_task->mcode;
    bytes_leidos =  read( fd, (void *)mem->dir, PAGINA_SIZE);
	
    for( i=1 ;  i < paginas_texto ; i++) {
		mem->next = umalloc_page (PAGINA_CODE, TASK_TEXT + (i*PAGINA_SIZE) , new_task->cr3);
		mem = mem->next;
        bytes_leidos =  read(fd, (void *)mem->dir , PAGINA_SIZE);
		if (bytes_leidos < 0) {	    //error de algo.... liberar memoria
		    actual->err_no = EIO;
		    close(fd);
		    return -1;
		}
    }

    //Pedir memoria para los datos (.DATA + .BSS)
    new_task->mdata = umalloc_page (PAGINA_DATA, TASK_DATA, new_task->cr3);
    if (!new_task->mdata)   //liberar
		return -1;
    for ( mem=new_task->mdata,i=1 ; i<paginas_datos ; i++, mem=mem->next ) {
        mem->next = umalloc_page (PAGINA_DATA, TASK_DATA + (i*PAGINA_SIZE) , new_task->cr3);
		if (!mem->next)	    //liberar
		    return -1;
    }

    //Levantar .DATA
    lseek(fd, sec_data.s_scnptr,SEEK_SET);
    for( i=0, mem=new_task->mdata ;   i < paginas_data  ;  i++, mem=mem->next ) {
		bytes_leidos =  read(fd, (void *)mem->dir, PAGINA_SIZE);
		if (bytes_leidos < 0) {	    //error de algo.... liberar memoria
		    actual->err_no = EIO;
		    close(fd);
		    return -1;
		}
    };

	// En esa p�gina extra que pedimos para datos, vamos a ubicar una llamada a EXIT
	// y los argumentos de main y variables de entorno que recibe la tarea
	// Me ubico en la ultima pagina (la cual usare para exit, argv, etc, etc)

    for( mem=new_task->mdata ;  mem->next  ;  mem=mem->next);

	unsigned char *ptr_exit = (unsigned char *) mem->dir;
	
	// Ubicamos en el wrapper el codigo de libreria routstd de llamada EXIT:
	*ptr_exit = 0xb8; 	// Codigo de operacion: "mov eax, "
	*(unsigned long *)(ptr_exit+1) = SYS_PROCESS | SYS_EXIT; 
	*(ptr_exit+5) = 0xbb; // "mov ebx, "
	*(unsigned long *)(ptr_exit+6) = 0; // parametro que recibe la funci�n EXIT... 0 en este caso
	*(ptr_exit+10) = 0xcd;	// int
	*(ptr_exit+11) = 0x50;	// 0x50

	// Hardcodeo la llamada al sistema sys_signal_check, la cu�l se ejecutar� luego de cada handler de se�a
	*(ptr_exit+12) = 0xb8; 	// Codigo de operacion: "mov eax, "
	*(unsigned long *)(ptr_exit+13) = SYS_SIGNALS | SYS_SIGNAL_CHECK; 
	*(ptr_exit+17) = 0xcd;	// int
	*(ptr_exit+18) = 0x50;	// 0x50
	*(ptr_exit+19) = 0x5e;	// pop esi	// Con estos popeos recupero los registros de prop�sito general
	*(ptr_exit+20) = 0x5f;	// pop edi  // que fueron pusheados antes de ejecutar un handler de se�al
	*(ptr_exit+21) = 0x5a;	// pop edx
	*(ptr_exit+22) = 0x59;	// pop ecx
	*(ptr_exit+23) = 0x5b;	// pop ebx
	*(ptr_exit+24) = 0x58;	// pop eax
	*(ptr_exit+25) = 0xc3;	// retn

	// Aloco la estructura que almacena info de se�ales
	TASK_SIGNALS_ALLOC(new_task);
	memset (TASK_SIGNALS(new_task), 0, sizeof(struct task_signals));
	TASK_SIGADDR(new_task) = GET_OFFSET(ptr_exit + 12) + mem->vdir;

//	new_task->sigcheck_addr = GET_OFFSET(ptr_exit + 12) + mem->vdir;
 
// Argumentos y demas facturetas	
	unsigned int argc = 0;
	char **argv, **envp;

	argv = NULL;
	envp = NULL;
	
	// Mando al fondo del Stack a envp
	unsigned long *qwe = (unsigned long *) (new_task->mstack->dir + 4096 - 4);
	*(qwe--) = (unsigned long) envp;
	*(qwe--) = (unsigned long) argv;
	*(qwe--) = (unsigned long) argc;
	*qwe = (unsigned long) mem->vdir;

	if (actual==pre_init_task) {				// Si el proceso es init, caso particular ya que init no tiene formato de tarea
	    TASK_PARENT(new_task) = actual;
		new_task->pid = get_new_pid();
		new_task->padre = pre_init_task;
		// A los procesos que "executa init" le inicializo la lista de hijos, ya que no la van a reemplazar
		// a init
		LIST_INIT (new_task->childs);
		// Agregarse como hijo de init
		LIST_ADD (actual->childs, brothers, new_task);
	}
	else {								// Si es cualquier otro proceso, el nuevo proceso debe heredar su pid y ppid
	    TASK_PARENT(new_task) = TASK_PARENT(actual);
	    new_task->pid = actual->pid;
		new_task->padre = actual->padre;
		// El nuevo proceso se hace cargo de todos los hijos
//		new_task->zombie_header = actual->zombie_header;
		//LIST_REPLACE(brothers, actual, new_task);
//		START_TICKS_COUNT();
		LIST_DEL(actual->padre->childs, brothers, actual);
		LIST_ADD_TAIL(actual->padre->childs, brothers, new_task);
//		END_TICKS_COUNT();
//		PRINT_TICKS_COUNT();
	}

    close(fd);

    // Despertamos la nueva tarea, recordemos que init_new_task crea las tareas en estado TASK_STOPPED
    despertar_task( new_task );

#ifdef RUNSTATS
  if ( getvar("debugexec") == 2 ) {
		long long int counterfinal;
		rdtscl(counterfinal);
		kprintf("0x%llx ",counterfinal-counter);
	}	
#endif

	// Recordemos que el proceso que ejecuto el exec debe terminar (excepto que sea el init)
	if (actual->pid!=1) {

		cli();
		sys_exit_mm();					// libero la memoria
		remover_task (actual);			// lo saco de la lista de tareas del scheduler
		kfree_page ((addr_t)actual);	// libero el task_struct
		sti();
		_reschedule();					
	}

	
    return OK;
}

*/


//! \brief Imprime el mensaje relacionado a un errorn
void sys_perror (char *str)
{
    str = convertir_direccion( str , actual->cr3_backup);
    perror(str);
}

/*! \brief modifica la prioridad de una tarea
 *  \param pid del proceso
 *  \param nueva prioridad
 */
int sys_renice (word pid, word prioridad)
{
//    kprintf("TEMP SYS_RENICE. PID: %d\tPRI: %d\n", pid, prioridad);
    task_struct_t *tmp;
    if ( (tmp = encontrar_proceso_por_pid(pid))==NULL ) {
		actual->err_no = ESRCH;
		return -1;
    }
    // Esto es solo a modo de prueba ya que en routix por ahora el TASK_STOPPED cumple las veces de TASK_INTERRUMIPLE
    if (prioridad < 1)
		dormir_task(tmp);
    tmp->prioridad = prioridad;    
    return OK;
}

//! \brief Devuelve el PID de la tarea actual
inline pid_t sys_get_pid (void)
{
    return actual->pid;
}

//! \brief Devuelve el PID de su padre
inline pid_t sys_get_ppid (void)
{
    return TASK_PPID(actual);
}

////////////////////////////////////////////////////////////////////////////////////////////////
//	Llamada al sistema Exit y funciones relacionadas
//
////////////////////////////////////////////////////////////////////////////////////////////////

//! Cantidad de procesos en estado Zombie
int zombie_queue_len = 0;


/*! \brief Termina un proceso y libera los recursos utilizados
 *  \param valor retornado al padre (o a qui�n recoja la condici�n de salida)
 *
 *  \relates	sys_exit_notify
 */
/*
 *  \relates	sys_exit_notify
 *  \relates	sys_exit_mm
 *  \relates	sys_wait
 */
void sys_exit (int valor)
{
	cli();
	actual->retorno = valor;	
	
	sys_exit_mm(actual);
	sys_exit_notify();
	sys_exit_fs();
	dormir_task(actual);
	actual->estado = TASK_ZOMBIE;
	sti();
	_reschedule();
} 

/*! \brief Libera todos los recursos de memoria utilizados por un proceso (excepto el task_struct)
 */
int sys_exit_mm(task_struct_t *task)
{
	// Libero la memoria utilizada por los handlers de se�ales (antes esto lo ejcutaba al final de esta funci�n
	// y a veces me cagaba todo.... averiguar porque !!!
	TASK_SIGNALS_FREE(task);
	
	struct user_page *aux, *tmp;
	// Liberar las paginas de c�digo
	for (aux=tmp=task->mcode ; aux && tmp ; aux=tmp) {
		tmp=aux->next;
		// Desmapear la direccion fisica de la logica
		kunmapmem(aux->vdir, task->cr3_backup);
	 	// Si Esta habilitada la variable de entorno "__exit", imprimir info
		if (getvar("__exit")==1)
			kprintf("Liberando Codigo dir: 0x%x\tvdir: 0x%x\n", aux->dir,aux->vdir);
		ufree_page(aux);
	}
	// Liberar de esta tarea las paginas de datos
	for (aux=tmp=task->mdata ; aux && tmp ; aux=tmp) {
		tmp=aux->next;
		kunmapmem(aux->vdir, task->cr3_backup);
		if (getvar("__exit")==1)
			kprintf("Liberando Datos dir: 0x%x\tvdir: 0x%x\n", aux->dir,aux->vdir);
		ufree_page(aux);
	}
	// Liberar de esta tarea las paginas de codig
	for (aux=tmp=task->mstack ; aux && tmp ; aux=tmp) {
		tmp=aux->next;
		kunmapmem(aux->vdir, task->cr3_backup);
		if (getvar("__exit")==1)
			kprintf("Liberando Stack dir: 0x%x\tvdir: 0x%x\n", aux->dir,aux->vdir);
		ufree_page(aux);
	}
	// Desmapear del directorio la pagina usada para los wrappers (de exit por ej)
	kunmapmem (TASK_TEXT - PAGINA_SIZE, task->cr3_backup);


	// Libero el directorio de p�ginas utilizado por la tarea
	kfree_page (task->cr3_backup);
	return 0;
}

/*! \brief notificaci�n al sistema de la terminaci�n de un proceso
 *  \nota  uso interno de System Call exit. Esta llamada deber� levantar todos los procesos hijos en estado
 *	zombie, y luego, ponerse el en estado zombie en la lista de procesos hijos de su padre (task->brothers)
*/	
int sys_exit_notify (void)
{
	// Aumento la cuenta que lleva el sistema sobre zombies (proposito de debug)
	zombie_queue_len++;

	int value;
	pid_t pid;
	task_struct_t *child, *aux;
	// Tomar la condici�n de salida de mis hijos zombies
	_LIST_FOREACH(child, actual->childs, brothers) {
		aux = LIST_NEXT(brothers, child);
		
		// Si el proceso est� zombie, tomo su condidci�n de salida y lo libero
		if (child->estado == TASK_ZOMBIE)	{
			if (getvar("exit")==1)
				kprintf("Esperando al hijo: %d\n", child->pid);
			// Liberar el primer hijo que se encuentre
			pid = sys_waitpid(-1,&value,WNOHANG);
			
			if (getvar("exit")==1)
				kprintf("EXIT_NOTIFY: termino hijo %d con: %d\n", pid, value);
		}
		// Si est� en otro estado, se lo paso a init
		else {
			LIST_DEL(actual->childs, brothers, child);
			LIST_ADD_TAIL(init_task->childs, brothers, child);
			child->padre = init_task;
		}
		
		child = aux;
	}
	// Envio la se�al SIGCHLD al padre
	_sys_kill(actual->padre, SIGCHLD);

	// Despertar a init	
	wakeup_init();
	return 0;
} 

/*! \brief Libera todos los recursos relacionados con archivos utilizados por el proceso
 */
int sys_exit_fs ()
{
	int i;
    for (i=0 ; i < MAX_FILES_POR_TAREA ; i++)
		if (actual->open_files[i])
			free(actual->open_files[i]);

	return 0;
}


/*! \brief Obtiene la condici�n de salida de un hijo en estado Zombie
 *  \param pid pid del hijo en cuesti�n o 0 para cualquier proceso hijo
 *  \param status lugar donde se guardar� la condici�n de salida del proceso
 *  \param options define el comportamiento de waitpid (momentaneamente solo implementamos WNOHANG)
*/	
pid_t sys_waitpid (pid_t pid, int *status, int options)
{
	_cli();
    status = convertir_direccion( status , actual->cr3_backup);

	struct task_struct_t *child;

	int zombie_found = 0;

	while (1) {
		// Recorrer la lista de procesos hijos, en busca de zombies
		LIST_FOREACH (child, actual->childs, brothers) {
			// Si el hijo no es zombie, seguir buscando
			if (TASK_STATE(child)!=TASK_ZOMBIE)
				continue;
			// Buscar cualquier hijo (pid=-1) o alguno en particular (pid>1)?	
			if (pid>1  &&  pid!=TASK_PPID(child))
				break;
			zombie_found = 1;
			break;
		}
		if (zombie_found==1)	
			break;
		if (getvar("__wait")==1)
			kprintf("SYS_WAIT: Mi PID es: %d\tNo encontre hijo zombie\n", actual->pid);

		// Si es no bloqueante, debo irme con -1 al no haberlo encontrado
		if (options==WNOHANG)	{		
			if (getvar("__wait")==1)
				kprintf("SYS_WAIT: Me voy por el WNOHANG....\n");
			actual->err_no = ECHILD;
			_sti();
			return -1;
		}
		_sti();
		_reschedule();	// espero un rato
		_cli();
	}
	
	// Mientras que aux queda apuntando al nodo anterior, tmp lo hace al nodo en cuesti�n
	pid_t child_pid = TASK_PID(child);
	if (getvar("__wait")==1) 
		kprintf("SYS_WAIT:Mi hijo zombie es: %d\n", child->pid);

	// Valor de retorno del hijo
	*status = child->retorno;
	remover_task (child);
	
	// Quitar al nodo de la lista
	LIST_DEL (actual->childs,brothers, child);
	
	// Liberar el task struct utilizado
	kfree_page ((addr_t)child);
//	free (aux);
	zombie_queue_len--;
	
	_sti();
	return child_pid;
}



//Cantidad de veces que la funci�n malloc llamo a morecore solicitandole una p�gina
extern word morecores;

void *punteros[1000];

/*! \brief muestra parametros internos con prop�sitos de debug
 *  \param parametro que desea mostrarse
 */
void sys_show (int valor)
{
	int xx;
	int mem_before = kmem_free();
	int veces;

	switch (valor) {
		case 1: 
			kprintf ("Cantidad de paginas asignadas via Morecores: %d\n", morecores);
			break;
		case 2:
			// Dump del contenido del blockcache
			show_cached_list();
			break;
		case 3:
			kprintf ("Cantidad de procesos zombies: %d\n", zombie_queue_len);
			break;
		case 4:
			if ((veces=getvar("veces"))==-1)
				veces = 10;

			for (xx=0 ; xx<veces ; xx++) {
				if ((punteros[xx]=malloc(300))==NULL) {
					kprintf("MALLOC ERROR\n");
					return;
				}
			}
			
			for (xx=0 ; xx<veces ; xx++) {
				free(punteros[xx]);
			}
			
			kprintf("Veces: %d\tMemoria antes: %d\tdespues: %d\n", veces, mem_before, kmem_free());
			break;

		case 5:
			num_mallocs = num_frees = 0;
			task_signals_alloc = task_signals_free = 0;
			umalloc_alloc = umalloc_free = 0;
			break;
		case 6:
			kprintf("Cantidad de Mallocs: %d\tFrees: %d\n", num_mallocs, num_frees);
			kprintf("Allocs de task_Signals: %d\tFrees: %d\n", task_signals_alloc, task_signals_free);
			kprintf("Allocs de User_page: %d\tFrees: %d\n", umalloc_alloc, umalloc_free);
			break;
		case 7:
			task_signals_alloc = task_signals_free = 0;
			break;
		case 8:
			kprintf("Cantidad de Mallocs de task_Signals: %d\tFrees: %d\n", task_signals_alloc, task_signals_free);
			break;

		case 9:
			mm_wrapper_dump_all();
			break;
		case 10:
			init_mm_wrapper();
			break;
		
		default: 

			break;
	}
			
}



int sys_clone(void *func)
{
	
	if(getvar("clone")!=-1) {
		kprintf("clone(): info Direccion de funcion a ejecutar: 0x%x\n", func);
	}


	addr_t thread_stack = thread_get_stack_address(actual);

	if(getvar("clone")!=-1) {
		kprintf("clone(): getting thread stack address: 0x%x\n", thread_stack);
	}

    thread_struct_t *new_thread; 

    new_thread = init_new_thread(DESC_CODE_USUARIO, DESC_DATA_USUARIO, (dword) func, thread_stack - 16, 0x202,\
		    "thread", START_PRIORITY);


	// Ambos threads(el creador y el creado) deben compartir el directorio de p�ginas, ya que si uno obtiene una
	// nueva p�gina de memoria el otro debe ser capaz de verla
	new_thread->cr3_backup = new_thread->cr3 = actual->cr3_backup;

    struct user_page *aux_mem;
    int i;
    
    // Asignaci�n de p�ginas de c�digo
    aux_mem = actual->mcode;
    for (i=0 ; i<actual->num_code ; i++) {
		// Incremento la cantidad de procesos que refieren a esta p�gina
    	aux_mem->count++;
    }
    
    // Asignaci�n de p�ginas de datos
    aux_mem = actual->mdata;
    for (i=0 ; i<actual->num_data ; i++) {
    	// Incremento la cantidad de procesos que refieren a esta p�gina
    	aux_mem->count++;
    }
    
    new_thread->tid = get_new_pid();

	// Obtengo una p�gina para el user stack, y la matcheo en el CR3 de la tarea
	new_thread->mstack = umalloc_page ( actual->mstack->flags, thread_stack - PAGINA_SIZE, new_thread->cr3 );
	
	if (new_thread->mstack==NULL) {
		kprintf("clone(): error umalloc_page NULL");
		return -1;
	}
	
	LIST_ADD_TAIL(actual->threads, threads, new_thread);

	actual->thread_count++;

	return 0;
}

