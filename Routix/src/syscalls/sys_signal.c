/*!  \addtogroup Syscalls
 	\page sys_signal.c
 	Syscalls relacionadas con el manejo de se�ales
*/

#include "routix/system.h"
#include "routix/debug.h"
#include "sys/syscalls.h"
#include "routix/syscalls.h"
#include "error.h"
#include "string.h"
#include "routix/task.h"
#include "routix/signal.h"

extern task_struct_t *actual;


int (*syscall_signals[MAX_SYSCALLS]) (void) = {
	(int (*) (void)) sys_kill,
	(int (*) (void)) sys_signal,
	(int (*) (void)) sys_sigaction,
	(int (*) (void)) sys_signal_check,
	(int (*) (void)) sys_sigprocmask

};
int _sys_kill(task_struct_t *proceso, int signo);
 
 
/*! \brief Env�a una se�al a un proceso
 *  \param pid pid del proceso receptor
 *  \param signo numero de la se�al a enviar
 *  \return -1 en caso de error
 *	\todo Evaluar sa_flags de la estructura sigaction
 *  \anchor sys_signal.c
*/
int sys_kill(pid_t pid, int signo)
{	
	if (getvar("__sig")==1)
		kprintf("Recibida la senal: %d para el proceso: %d\n", signo, pid);
	// La se�al que se intenta enviar, no existe
	if (signo > SIGMAX) {
		actual->err_no = EINVAL;
		return -1;
	}
	
	task_struct_t *proceso;
	proceso = encontrar_proceso_por_pid(pid);

	return _sys_kill(proceso, signo);
}	


/*! \brief Env�a una se�al a un proceso
 *  \param proceso task_struct del proceso que recibe la se�al
 *  \param signo numero de la se�al a enviar
 *  \return -1 en caso de error
 *  \note esta funcion pertenece al kernel. No verifica la validez de sa se�al ya que esa es tarea
 *  		 de sys_kill. Usar con precauci�n y solo en Kernel Mode.
 *	\todo Analizar POSIX y verificar si cuando un handler vale SIG_IGN, se marca la se�al como
 * 		  pendiente pero no se la ejecuta, o si siquiera se marca como pendiente
 */
int _sys_kill(task_struct_t *proceso, int signo)
{
	// Si el proceso no existe
	if (!proceso) {
		actual->err_no = ESRCH;
		return -1;
	}

	// Por ahora init solo puede recibir SIGCHLD
	if (proceso==init_task && signo!=SIGCHLD)	{
		actual->err_no = ESRCH;
		return -1;
	}

	// Si la se�al no se la est� ignorando, marcarla como pendiente
	if (TASK_SIGNAL_HANDLER(proceso, signo) != SIG_IGN) {
		TASK_SIGPENDING(proceso) = TASK_SIGPENDING(proceso)  | (1 << signo);
		// Aqui "encolo" las se�ales pendientes de un mismo tipo
		TASK_SIGNAL_COUNT(proceso, signo)++;
	}
	return 0;
}


/*! \brief establece un handler para una se�al (llamada signal)
 *  \param signo n�mero de la se�al
 *  \param func  direcci�n del handler
 *  \return handler previo
 *	\todo Evaluar sa_flags de la estructura sigaction
 */
void *sys_signal (int signo, void (*func)()) 
{
	void (*old_func)() = TASK_SIGNAL_HANDLER(actual, signo);
	// Verificar si la funci�n "func" pertenece al espacio de direcciones del usuario
	TASK_SIGNAL_HANDLER(actual, signo) = func;
	if (getvar("__sig")==1)
		kprintf("Direccion de la funcion especificada: 0x%x\n", func);
	return old_func;
}

/*! \brief Corrige el stack antes de recuperar todos los registros del contexto
 */
void sys_signal_correct(void)
{
	//kprintf("SYS_SIGNAL_CORRECT");
	
	struct int_regs_ext *contexto = GET_CONTEXT(actual);
	
	contexto->esp += 4;
	
	//__debug();
}


/*! \brief fuerza un chequeo de se�ales pendientes, y en caso de haberlas, ejecuta sus handlers
 *  \note  no tiene ninguna utilidad para el usuario. Es ejecutada luego de la ejecuci�n de cada handler
 *		   es necesaria ya que debe cambiarse la m�scara de se�ales por cada handler
 *	\todo Evaluar sa_flags de la estructura sigaction
 */
void sys_signal_check (void)
{
	struct int_regs_ext *context = GET_CONTEXT(actual);

	// Porque es esto ??? Bla bla... no estoy seguro...
	context->esp += 4;

	// Obtener la direcci�n f�sica del stack de la tarea
	unsigned long *user_ptr = (unsigned long *) convertir_direccion((void *)context->esp, actual->cr3_backup); 

	// Obtengo el valor de sigmask valido antes de ejecutar este handler que acaba de finalizar
	TASK_SIGMASK(actual) = *(user_ptr++);

	// Si no hay otras sela�es pendientes... reacomodo el stack y retorno
	if (check_sigpending(actual, 0)<1)	{
		context->esp += 4;
//		kprintf("SYS_SIGNAL_CHECK: No hay otras pendientes\n");
	}
	// Si hay otra se�al pendiente, pongo el EIP del contexto para que apunte al nuevo handler
	else {
//		kprintf("SYS_SIGNAL_CHECK: HAY !!! pendientes\n");
	}

}

/*! \brief establece o verifica la m�scara actual
 *  \param flag indica si se va a establecer, modificar o ambas
 *  \param set nueva mascara
 *  \param old_set aqui se colocar� la antigua m�scara
 *  \return 0 en caso de �xito
 *	\note Llamada al sistema sigpromask (ver m�s datos en p�ginas man 2 de nix*)
 *  \note increiblemente esta llamada cumple con el item RATIONALE de POSIX :-)
 */
int sys_sigprocmask(int flag, sigset_t *set, sigset_t *old_set)
{
	old_set = (old_set == NULL) ? NULL : convertir_direccion(old_set, actual->cr3_backup);
	set = (set == NULL) ? NULL : (sigset_t *)convertir_direccion(set, actual->cr3_backup);
	
	// Si old_set no es NULL, retorno en el la m�scaraa ctual
	if (old_set) {
		*old_set = TASK_SIGMASK(actual);
		if (!set)
			return 0;
	}

		
	switch (flag) {
		case SIG_BLOCK:
			TASK_SIGMASK(actual) |= *set;
			break;
		case SIG_UNBLOCK:
			TASK_SIGMASK(actual) = TASK_SIGMASK(actual) & (~*set);
			break;
		case SIG_SETMASK:
			TASK_SIGMASK(actual) = *set;
			break;
		default:
			actual->err_no = EINVAL;
			return -1;

	}
			
	//kpanic("sigprocmask");
	return 0;
}

/*! \brief establece un handler para una se�al (llamada signal)
 *  \param signo n�mero de la se�al
 *  \param act sigaction que se asosiar� a la se�al signo
 *  \param oact lugar donde se retorna el sigaction que se encontraba asociado a signo
 *  \return 0 en caso de �xito
 *	\todo evaluar sa_flags de la estructura sigaction
 */
int sys_sigaction (int signo, struct sigaction *act, struct sigaction *oact) 
{
	
	if (signo >= SIGMAX)	{
		actual->err_no = EINVAL;
		return -1;
	}
	
	oact = (oact == NULL) ? NULL : convertir_direccion(oact, actual->cr3_backup);
	act = (act == NULL) ? NULL : convertir_direccion(act, actual->cr3_backup);

	if (oact)
		memcpy (oact, &(TASK_SIGNAL(actual, signo)), sizeof (struct sigaction));
	
	if (act)
		memcpy (&(TASK_SIGNAL(actual, signo)), act, sizeof (struct sigaction));
	
	return 0;
}

