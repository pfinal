/*!  \addtogroup Syscalls
	\page sys_time.c
	Funciones de control y administraci�n de tiempo y timers.
*/


#include "routix/system.h"
#include "routix/paging.h"
#include "routix/segm.h"
#include "routix/debug.h"
#include "sys/syscalls.h"
#include "routix/syscalls.h"
#include "routix/elf.h"
#include "routix/file.h"
#include "error.h"
#include "routix/timer.h"
#include "routix/atomic.h"
#include "routix/kalloc.h"
#include <routix/kstdio.h>

#ifndef __TASK
#include "routix/task.h"
#endif


//Nuevas
extern int sys_process (void);
extern int sys_console (void);
extern int sys_timer (void);
extern int sys_mem (void);

extern task_struct_t *actual;

// Acceso al listado
//extern timer_t *timer_inicio;
extern dword jiffies;



// Vector de funciones de llamadas al sistema (grupo Timer)
int (*syscall_timer[MAX_SYSCALLS]) (void) = {
	(int (*) (void)) sys_sleep,
	(int (*) (void)) sys_proc_dump,
	(int (*) (void)) sys_kill,
	(int (*) (void)) sys_usleep,
	(int (*) (void)) sys_proc_dump_v,
	(int (*) (void)) sys_timer_dump
};



// Funcion llamada por el timer en el retorno para sys_sleep y sys_usleep
static void despertar(timer_t *info)
{
  if ( info != NULL ) {
    despertar_task(TIMER_PROCESS(info));
  }
}


int sys_sleep(int segundos)
{
 sys_usleep(segundos*1000000);
 return OK;
}

int sys_usleep(int usegundos)
{
	timer_t *timer;

	// Creamos el timer
	if ( (timer=create_timer(USECONDS_TO_TICKS(usegundos), actual, despertar, NULL))==NULL ) {
		return -1;
	}
 
	// Dormimos la tarea actual
	dormir_task(actual);

	// Switcheamos de proceso mediante la macro _reschedule quien realiza una llamada
	// a la interrupci�n 0x51
	_reschedule();

	// Volvemos ! el timer se venci�. Este es nuestro punto de retorno.
	// todav�a estamos en modo kernel en la llamada al sistema

	return OK;
}



int sys_proc_dump(void)
{
 task_struct_t *tmp;

 char *estados[] = { "TASK_RUNNING", "TASK_STOPPED", "TASK_SLEEPING", "TASK_ININTERRUMPIBLE", "TASK_ZOMBIE", \
					"TASK_CLEAN" };

 kprintf("pid ppid descripcion estado pri cuenta cpu sigpending sigmask\n");
 for (tmp = tareas_inicio; tmp != NULL ; tmp=tmp->proxima ) {

  kprintf("%d\t%d\t%s", tmp->pid, tmp->padre->pid, tmp->descripcion);
  kprintf(" %s %d %d %d",estados[tmp->estado], tmp->prioridad,tmp->cuenta,tmp->tiempo_cpu);
  kprintf(" 0x%x\t\t0x%x\n", TASK_SIGPENDING(tmp), TASK_SIGMASK(tmp));

 }

 return 0;
}

// Sys Proc Dump Verbose
int sys_proc_dump_v(int pid)
{
    task_struct_t *tmp;
    //Ubicar el pid en la lista de procesos

    if ( (tmp=encontrar_proceso_por_pid(pid))==NULL ) {
		actual->err_no = ESRCH;	    //proceso no existe
        return -1;
    }

	if (tmp->estado == TASK_ZOMBIE) { //Si el proceso es Zombie, no tiene que mostrar
		actual->err_no = ESRCH;	    
		return -1;
	}
	
    kprintf("Descripcion: %s\tPID: %d\tThreads: %d\n", tmp->descripcion, tmp->pid, tmp->thread_count);
    kprintf("P�ginas utilizadas: ");
    kprintf("Code: %d\tData: %d\tUserStack: %d", tmp->num_code, tmp->num_data, tmp->num_stack);
    kprintf("\nCR3: 0x%x\t", tmp->cr3_backup);
    kprintf("Kernel Stack: 0x%x\t", tmp->esp0 & 0xfffff000);
    struct user_page *mem;
    kprintf("\nCode Pages:\n");
    for(mem=tmp->mcode ; mem ; mem=mem->next)
	kprintf("Adress: 0x%x\tLinear address: 0x%x\n", mem->dir, mem->vdir);
    kprintf("Data Pages:\n");
    for(mem=tmp->mdata ; mem ; mem=mem->next)
	kprintf("Adress: 0x%x\tLinear address: 0x%x\n", mem->dir, mem->vdir);
    kprintf("UserStack Pages:\n");
    for(mem=tmp->mstack ; mem ; mem=mem->next)
	kprintf("Adress: 0x%x\tLinear address: 0x%x\n", mem->dir, mem->vdir);
	kprintf("Hijos de la tarea:\n");
	task_struct_t *hijo;
/*	LIST_FOREACH(hijo, actual->childs, brothers) {
		kprintf("PID: %d\t", TASK_PID(hijo));
	}*/
	 char *estados[] = { "R", "ST", "SL", "U", "Z", \
					"C" };
	
	for(hijo=tmp->childs.next ; hijo!=NULL; hijo=hijo->brothers.next)
		kprintf("PID: %d-%s\t", TASK_PID(hijo), estados[hijo->estado]);
	kprintf("\n");

    return OK;
}	



int sys_timer_dump(void)
{
	timer_dump();

	return 1;
}

