/*!  \addtogroup Syscalls
	\page syscalls.c
	Core de las llamadas al sistema (junto con syscalls.asm).
*/


#include "routix/system.h"
#include "routix/paging.h"
#include "routix/segm.h"
#include "routix/debug.h"
#include "sys/syscalls.h"
#include "routix/syscalls.h"
#include "routix/elf.h"
#include "routix/file.h"
#include "error.h"
#include "routix/timer.h"
#include <routix/kstdio.h>
#ifndef __TASK
#include "routix/task.h"
#endif


//Nuevas
extern int sys_process (void);
extern int sys_console (void);
extern int sys_signals (void);
extern int sys_timer (void);
extern int sys_mem (void);
extern int sys_misc (void);		//Grupo de llamadas al sistema provisorias (cualquier fruta... va ac�).

extern task_struct_t *actual;


//! Vector de grupos de llamadas al sistema
int (*syscall_group_vector[MAX_SYSCALLS]) (void) = {
	(int (*) (void)) sys_process,	
	(int (*) (void)) sys_console,	
	(int (*) (void)) sys_signals,				
	(int (*) (void)) sys_timer,	
	(int (*) (void)) sys_mem,	
	(int (*) (void)) sys_misc	
};

/*! \brief Definen la cantidad m�xima de llamadas al sistema por grupo 
 *  Son pasadas a una variable para poder levantar su valor desde syscalls.asm
 */
unsigned int syscall_group_max[MAX_SYSCALLS] = {
	SYS_PROCESS_MAX,
	SYS_CONSOLE_MAX,
	SYS_SIGNALS_MAX,
	SYS_TIMER_MAX,
	SYS_MEM_MAX,
	SYS_MISC_MAX
};

/*! \brief Se ejecuta cuando se hace una llama a una syscall inexistente
 *  \todo Verificar que dice POSIX respecto a realizar llamadas erroneas (probablemente se genera alguna se�al)
 */
int sys_no_existe (dword numero)
{
    kprintf("SysCall Inexistente: Grupo %d\tLlamada %d\n", numero>>16 , (word)numero);
    actual->err_no = ENOSYS;
    return -1;
}	

