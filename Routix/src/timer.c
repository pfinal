/* timer.c */

/* Manejo de la interrupcion de timertick y timers */

#include <routix/system.h>
#include <routix/8254.h>
#include <routix/8259.h>
#include <routix/debug.h>
#include <routix/task.h>
#include <routix/kalloc.h>
#include <routix/time.h>
#include <routix/sched.h>
#include <routix/atomic.h>
#include <routix/timer.h>
#include <sys/list.h>

// ojo que los jiffies podrian tener un overflow !
// a los 497 dias de estar corriendo
dword jiffies=0;

// Puntero al inicio de la lista de timers
volatile timer_t *timer_inicio=NULL;

// Lista de timers
LIST_NEW(timer_t) timer_list = LIST_INITIALIZER;

void actualizar_timers(void);
inline static int insert_timer(timer_t *nuevo);
inline static int remove_timer(timer_t *timer);


void timertick_handler()
{

 endOfInterrupt();
 
 // Incrementamos el contador de jiffies
 jiffies++;

 // Actualizamos el reloj
 if ( ! (jiffies % FINTERRUPCION) ) actualizar_reloj(jiffies);

 // Revisamos los timers
 actualizar_timers();

  // Chequeamos si el floppy tiene algo pendiente, en realidad colocarlo
 // aca tiene un bajo rendimiento, solo para pruebas
 floppy_procesar_buffer();

 // Verificamos si hay actualmente una tarea corriendo antes de modificar
 // sus estadisticas
 if ( actual ) {
		// Actualizamos la cuenta del proceso actual
		actual->cuenta -= 1;
		actual->tiempo_cpu++;

		// Si se venci� la cuenta, la reestablecemos y pasamos el control al scheduler
		if ( actual->cuenta <= 0 ) {
		// Reiniciamos la cuenta
		actual->cuenta = actual->prioridad;
		scheduler();
		__asm__ ("nop");
		}
 }

 // Si no la hubiese, devolvemos el control al scheduler
 else {
	scheduler();
 }

}

/*
void actualizar_timers(void)
{
  timer_t *tmp;

  // Chequeamos si hay timers activos
  if ( timer_inicio == NULL ) {
   return;
  } 

  for ( tmp=timer_inicio; tmp != NULL; tmp = tmp->proximo ) {

    // Debe estar pendiente (ver timer.h) y haber vencido su cuenta
    if ( (tmp->estado==PENDIENTE) && (--(tmp->ticks) <= 0) ) {
	    
      // Ejecutamos la funcion establecida
      (*(tmp->func))(tmp);

      // Seteamos su estado como finalizado
      tmp->estado=FINALIZADO;
    }
		 
  }
 
}
*/


void actualizar_timers(void)
{
  timer_t *tmp;
	timer_t *toclean;

	// Recorremos la lista procesando los timers vencidos hasta encontrar uno m�s grande
	for(tmp=HEADER_NEXT(timer_list); tmp ;) {

		if ( TIMER_TICKS(tmp) > jiffies )
			break;

		// Llamamos a la funci�n seteada en el timer
		(*(TIMER_FUNCTION(tmp)))(tmp);

		// lo almacenamos temporalmente
		toclean=tmp;

		// buscamos el nodo siguiente
		tmp=LIST_NEXT(timer_list,tmp);
			
		// y lo eliminamos
		clean_timer(toclean);

	}

}


spinlock_t timer_lock = 1;

/*
int insertar_timer(timer_t *nuevo)
{
  // Deshabilitamos Interrupciones
  cli();
	
  timer_t *tmp;
	
  if ( nuevo == NULL ) { return 0; }

  // Nos paramos al ppio de la lista
  tmp = timer_inicio;

  if ( timer_inicio == NULL ) { timer_inicio = nuevo; }

  else {
 
    // Buscamos la �ltima tarea
    for ( tmp = timer_inicio; tmp->proximo != NULL ; tmp = tmp->proximo );

    // Nos colgamos de ella
    tmp->proximo = nuevo;
  }

  // La nueva tarea queda apuntando a NULL
  nuevo->proximo = NULL;

  // Pendiente de ejecucion
  nuevo->estado=PENDIENTE;

  // Habilitamos Interrupciones
  sti();

  return 1;
}
*/


inline static int insert_timer(timer_t *nuevo)
{
	// Deshabilitamos Interrupciones
	START_ATOMIC_OPERATION;
	
	timer_t *tmp;
	
	if ( nuevo == NULL ) { return 0; }

	// Buscamos el primer nodo que posea un tiempo mayor al tiempo del nodo nuevo
	// de manera que la lista siempre este ordenada de menor a mayor

	// Si la lista est� vacia lo insertamos al ppio
	//if ( LIST_EMPTY(timer_list) ) {
	//	LIST_ADD(timer_list,timer_list,nuevo);
	//}

	// Sino buscamos en el listado de timers
	//else {
	
		LIST_FOREACH(tmp,timer_list,timer_list) {

			// Dejamos ">=" como criterio as� la b�squeda concluye lo antes posible, ya que no importa
			// si queda al ppio o final de un grupo de nodos con el mismo tiempo.
			if ( TIMER_TICKS(tmp) >= TIMER_TICKS(nuevo) )
				break;
		}

		// Si no encontramos ning�n nodo con tiempo mayor colocamos nuestro nuevo nodo al final de la lista
		if ( tmp==NULL ) {
			LIST_ADD_TAIL(timer_list,timer_list,nuevo);
		}

		// lo insertamos antes de nuestro nodo encontrado
		else {
			LIST_INSERT_BEFORE(timer_list,timer_list,tmp,nuevo);
		}

	//}

	// Habilitamos Interrupciones
	FINISH_ATOMIC_OPERATION;

	return 1;
}

/*
int remover_timer(timer_t *timer)
{
  cli();

  timer_t *tmp;

  // Es el primer timer ?
  if ( timer == timer_inicio ) {
    timer_inicio = timer->proximo;
    sti();
    return 0;
  }
 
  // Buscamos nuestro timer entonces
  for ( tmp=timer_inicio; (tmp->proximo != timer) && (tmp != NULL) ; tmp = tmp->proximo ) ;

  // Si no encontramos el timer devolvemos error
  if ( tmp == NULL ) {
    // Lo tengo que reemplazar por la constante correcta seg�n la definici�n de errno.h
    sti();
    return -1;
  }

  else {
    tmp->proximo = timer->proximo;
  }

  // Lo tengo que reemplazar por la constante correcta seg�n la definici�n de errno.h
  sti();
  return 0;

}

*/

inline static int remove_timer(timer_t *timer)
{
	// Deshabilitamos Interrupciones
	START_ATOMIC_OPERATION;

  timer_t *tmp;

	// Este bloque de cod es opcional y permite que se realice un chequeo adicional
	// a la hora de borrar un timer, verificando que exista en la lista
	#ifdef SECURE_TIMER_OPER
	// Buscamos si realmente existe el timer, para evitar problemas de inconsistencias
	LIST_FOREACH(tmp,timer_list,timer_list) {
		if ( tmp==timer )
			break;
	}

	// EL timer no existe en la lista !!
	if ( tmp==NULL ) {
		FINISH_ATOMIC_OPERATION;
		return -1;		
	}

	#endif

	// Lo eliminamos
	LIST_DEL(timer_list,timer_list,timer);

	// Habilitamos Interrupciones
	FINISH_ATOMIC_OPERATION;

  return 0;

}


inline timer_t *create_timer(dword ticks, task_struct_t *proceso, void (*func)(struct timer_t *info), void *data)
{
  //timer_t *timer = (timer_t *) malloc(sizeof(timer_t));
  timer_t *timer = (timer_t *) MALLOC(MM_WRAPPER_TIMER,sizeof(timer_t));

	if ( timer==NULL)
		return NULL;

	TIMER_TICKS(timer)=jiffies+ticks;
	TIMER_PROCESS(timer)=proceso;
	TIMER_FUNCTION(timer)=func;
	TIMER_DATA(timer)=data;

  insert_timer(timer);
	
  return timer;
}

inline int clean_timer(timer_t *timer)
{
	// Chequeamos primero que recibimos un timer	
	if ( timer==NULL ) {
		kprintf("clean_timer: Cant remove timer\n");	
		return -1;
	}
	
	// Lo sacamos de la lista de timers
	
	// Si no lo pudimos remover (no existe por ejemplo) retornamos con error
	if ( remove_timer(timer) == -1 ) {
		kprintf("clean_timer: Cant remove timer\n");	
		return -1;
	}
	
  // Liberamos la memoria la memoria
  //free(timer);
  FREE(MM_WRAPPER_TIMER,timer);

  return 1;
}


/*! \brief Solo para debug, imprime en pantalla el listado de timers activos
 */
inline void timer_dump(void)
{
	timer_t *tmp;

  START_ATOMIC_OPERATION;

  kprintf("Pid\n");
	LIST_FOREACH(tmp,timer_list,timer_list) {
    kprintf("%d\n", TASK_PID(TIMER_PROCESS(tmp)));
	}

	FINISH_ATOMIC_OPERATION;

}
