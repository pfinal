/*!  \addtogroup Scheduler
	\page sched.c
	Contiene la base del scheduler y la tarea Init
*/

#include <routix/system.h>
#include <routix/segm.h>
#include <routix/paging.h>
#include <routix/kalloc.h>
#include <routix/task.h>
#include <routix/debug.h>
#include <routix/kstdio.h>
#include <error.h>
#include <routix/syscalls.h>
#include <sys/list.h>
#include <string.h>
#include <routix/signal.h>

void idle_task(void);
void tarea_init(void);	    //Tarea INIT

int TASK_STACK_SIZE = 1;

// Puntero a la lista enlazada de tareas
task_struct_t *tareas_inicio=NULL;

// Puntero a la tarea actual
task_struct_t *actual=NULL;

// Cantidad de tareas activas
extern volatile int tareas_activas;


task_struct_t *init_task, *pre_init_task;

void start_scheduler(void)
{
	/* Por ahora usamos la memoria fisica directamente, total esta mapeada
	 * hasta que reubiquemos el codigo y datos de las tareas a una direcci�n
	 * virtual particular */
	
	// Inicializo el tss con valores nulos
	inicializarTss(&tss, 0, 0, 0, 0, 0 );	
	
	// Tarea fundamental del sistema ! INIT_TASK corre cuando no hay nada para correr
	addr_t INIT_stack = kmalloc_page();
	
	
	init_task=init_new_task(DESC_CODE, DESC_DATA, (dword) &tarea_init, INIT_stack + 4096, 0x202, "init", 50);
	pre_init_task = init_task;


	init_task->pid = 1;
	// Init es padre e hijo (santisima trinidad)... para evitar problemas al hacer init->padre
	init_task->padre = init_task;
//	LIST_INIT(init_task->zombie_header);
 
 	// Aloco la estructura de se�ales de init y la incializo (cuidado !!! esto NO ANDA !!!!!!!)
//	TASK_SIGNALS_ALLOC (init_task);
//	TASK_SIGNALS_INIT (init_task);
 
	// Despertamos la nueva tarea, ya que recordemos init_new_task crea la nueva tarea en estado STOPPED
	despertar_task(init_task);
	
	// La tarea en ejecuci�n ser� init_task
	actual = tareas_inicio;
 
}


int execve (char *, char **, char **);
void tarea_init()
{
	
  // Inicializamos la fat
	extern dev_fat_t dev_fat[1];		//Estructura para dispositivos con fs FAT

	dev_fat[0].boot_leido = FALSE;		
	dev_fat[0].fat_levantada = FALSE;

	TASK_STACK_SIZE = 1;	
	if (execve("init.bin", NULL, NULL)==-1)
		kpanic("No se pudo cargar init.bin");


	init_task = encontrar_proceso_por_pid(2);		// Init.bin
	tareas_inicio = init_task;
	sys_renice(2,1);
	TASK_PID(init_task) = 1;
	
	_reschedule();
	while (1) {
		kprintf("Esto no debe imprimirse ya que esta tarea no se schedulea");
	}

}	


/* Pasa los scan codes del buffer en crudo, a los ASCII con combinaciones de ALT, SHIFT y CTRL */
extern void leer_buff_teclado (void);


// Busca la pr�xima tarea activa
void scheduler()
{
	task_struct_t *tmp;
	
	leer_buff_teclado();
	
	if ( tareas_activas ) {
	
		// Volvemos al principio
		if (  (actual==NULL) || ((tmp = actual->proxima ) == NULL)  ) { tmp = tareas_inicio; }
	
		// Buscamos una tarea para correr
		while ( tmp->estado != TASK_RUNNING ) {
	   		if ( (tmp = tmp->proxima) == NULL ) { tmp = tareas_inicio; }
		}
		actual = tmp;
		
		// Si esta seteada la variable de entorno signals en 1 no verificar se�ales
		if (getvar("signals")!=1) {
			// Buscar se�ales pendientes, y ejecutar handler
			check_sigpending(actual, 1);
		}
	}
	
	else {
		actual=NULL;
	 	_sti();
	 	while ( ! tareas_activas ) ;
	 	_cli();
	 	scheduler();
	}

}

