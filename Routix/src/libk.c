/* libk.c */

/* Aca van todas las funciones que inician la llamada al sistema. Usadas, por ejemplo, por INIT */

#include "routix/system.h"
#include "routix/time.h"
#include "routix/paging.h"
#include "routix/task.h"
#include "sys/syscalls.h"
#include "routix/syscalls.h"
#include <routix/kstdio.h>

#include <stdarg.h>

// Constantes
#define HEXADECIMAL 16

// Macros
#define PRINTn(num,base,padding) (printn_all(num,base,padding,0));


int exec (char *);
char getascii_video (char c);
void printn( unsigned int num, int base);
void printn_all( unsigned int num, int base, unsigned int padding,unsigned int anidamiento);



int execve (char *tarea, char **argv, char **envp)
{
   	int retorno;
    _syscall3(SYS_PROCESS | SYS_EXECVE, retorno, tarea, argv, envp);

    return retorno;
}	


void kprintf ( char *string, ...)
{
 char *p=string;
 char *d;
 char car;

 unsigned int i;
 long long int lli;
 int padding;
	
 va_list argumentos;

 va_start(argumentos, string );

 for ( p=string; *p ; p++ ) {
  if ( *p != '%' ) {
   putchar(*p);
   continue;
  }
  
	switch (*++p) {

		case 'c':
	    car=va_arg(argumentos, int);     
	    putchar( (char) car);
	    break;

	  case 'x':
	    i = va_arg(argumentos, unsigned int );
	    printn(i,16);
	    break;


		case 'd':
	   i = va_arg(argumentos, int);
		 if (i > (0xffffffff/2) ) {
			putchar('-');
			printn(~i+1,10);
			break;
		 }
	     printn(i,10);
	     break;

	  case 'u':
	    i = va_arg(argumentos, unsigned int);
	    printn(i,10);
	    break;
		 
		case 'o':
	   i = va_arg(argumentos, unsigned int);
     printn(i,8);
     break; 

		case 's':
		  d = va_arg(argumentos, char *);
		  puts(d);
	    break;

	
		// Tenemos el caso de querer imprimir un long long (8 bytes = 64bits)
		// para ello tenemos que pensar que dado un n�mero, por ej.:
		//	2217 = 0x08A9 = 100010101001
		// supongamos a modo de an�lisis que tenemos que imprimir estos 2 bytes y
		// que nuestra capacidad de manejo (en registros) es de 1 byte.
		//	El MSB= 0x08 = 8
		//	y	LSB=0xA9	= 169
		// mientras que deber�amos leer:
		//	MSB = 22 = 0x16 
		//	LSB = 17 = 0x11
		//
		//	MSB = 0x08 --> 0x16 => dif = 0x16 - 0x08 = 0x0e
		//	LSB = 0xa9 --> 0x11	=> dif = 0x11 - 0xa9 = neg 
		// 
		// Otro ejemplo:
		//	3598 = 0x0E0E = 111000001110
		//
		//	MSB = 0x0E --> 0x35	=> dif = 0x35 - 0x0e = 0x27
		//	LSB = 0x0E --> 0x98	=> dif = 0x98 - 0x0e = 0x8a
		// 
		// Por ahora no encuentro una forma sencilla de imprimir n�meros long long
		// en decimal, realizamos la implementaci�n en hexa.
		// 
		case 'l':

			if ( *++p != 'l' )
				break;

			switch ( *++p ) {

				case 'x':
					lli = va_arg(argumentos, long long int);
		
					// Si los 4 bytes m�s significativos no son nulos el debemos agregar
					// ceros en el menos significativo
					if ( lli>>32 )
						padding=8;

					// Si los 4 bytes m�s significativos son nulos no importa el padding, 
					// pero lo colocamos en 1 para que cuando sea 0 imprima por lo menos
					// ese caracter 
					else
						padding=1;
						
					PRINTn( (unsigned int) (lli>>32), HEXADECIMAL, 0);
					PRINTn( (unsigned int) lli, HEXADECIMAL, padding);
					break;

			}
			
			break;
	     
	     

		default:
			putchar(*p);
			break;

  }
	
 }
  
 va_end(argumentos);
}

void printn( unsigned int num, int base)
{
 unsigned int div;
 if ( (div=num/base) ) printn(div,base);
 putchar( getascii_video(num%base) );
}


char getascii_video ( char c )
{
 char valor = '0' + c;

 if ( valor > '9' ) valor += 7;
 return valor;
}

//! Obtiene la cantidad de strings que contiene el vector
inline unsigned int get_vector_elements (char **p)
{
	unsigned long i;
	for ( ; *(p+i) ; i++);
	return i;
}

/* Permite imprimir un entero con padding
 *
 * function printn_all(numero, base, padding, anidamiento)
 *
 * division = numero/base;
 * resto		= numero%base;
 *
 * * Con resto levantamos el �ltimo d�gito, ya que la divisi�n puede ser
 * * nula, ej.: 7/16 = 0 pero 7%16 = 7
 * if ( division || resto ) {
 *	printn_all(division,base,padding,anidamiento+1)
 *	imprimir el caracter numero%base;
 * }
 *
 * else {
 *	while( padding-- > anidamiento )
 *		imprimir el caracter '0'
 * }
 *
 */ 
void printn_all( unsigned int num, int base, unsigned int padding,unsigned int anidamiento)
{
	unsigned int div;
 
	if ( (div=num/base) || (num%base) ) {
		printn_all(div,base, padding, anidamiento+1 );
		putchar( getascii_video(num%base) );
	 }

	else {

		while ( padding-- >anidamiento)
			putchar( getascii_video(0) );
	}

}

//! Cuenta la cantidad de elementos que tiene un vector de punteros
inline int count_elements (char **vector)
{
	int i;
	for ( i=0 ; vector[i]!=NULL && i<10 && vector; i++);
	return i;
}
