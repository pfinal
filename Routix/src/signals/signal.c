/*!  \addtogroup Signals
	\page signal.c
	Implementaci�n en desarrollo
	\ref Ver tambi�n sys_signal.c
*/
#include <routix/system.h>
#include <routix/task.h>
#include <signal.h>
#include <routix/signal.h>
#include <sys/types.h>
#include <routix/kstdio.h>
#include <error.h>
#include <routix/debug.h>
#include <routix/syscalls.h> 
 
void exec_sig_default (task_struct_t *task, int signo);
void sig_kill(task_struct_t *task);

/*! \note Notas sobre se�ales: 
 * 3-May-2004
 * antes de schedulear la tarea, debo verificar si tengo se�ales pendientes. (en caso de
 * haber, debo ejecutar el handle correspondiente.
 * Como hago para que cuando termino de ejecutar el handle, la tarea siga ejecutandose donde
 * se encontraba ? Para ser m�s claro, supongamos que la tarea est� esperando en un while
 * la llegada de una se�al de tipo SIGUSR1, con un c�digo como el que sigue
 * 		while (flag_siguser1==0);
 *      continuar_ejecucion....
 * luego de ejecutado el handler, la tarea deber�a hacer la verificaci�n de condici�n del
 * while, y seguir...
 * En este momento se me ocurri� (momentaneamente) que si la funci�n check_sigpending encuentra
 * una se�al pendiente, pushee el IP de la tarea al stack, y ejecute el handler utilizando
 * ese mismo stack de usuario, para que cuando el handler finalize, la ejecuci�n siga 
 * "armonicamente". Para recuperar el valor del IP, creo, deber�a utilizar una estructura
 * llamada "int_regs_ext" creada para el uso de la llamada fork.
 * Con la siguiente linea, conseguir�a cargar el contexto de una tarea con esa estructura
 *  new_ext = (struct int_regs_ext *) \
 *            ((new_task->esp0 & 0xfffff000) + PAGINA_SIZE - sizeof(struct int_regs_ext));
 * Ser�a bueno implementar una macro GET_CONTEXT(task) para obtener facilmente el contexto
 * y asi modularizar bien el c�digo.
 * Yo pensaba en saltar desde check_sigpending a la funci�n apuntada por sa_handler, el tema es que
 * aqu� estoy en modo kernel y no me convece la idea de switchear el stack.
 * Me parece ser�a mejor apuntar con el eip del contexto al handler, y colocar al tope del stack
 * la direcci�n ip que se encuentra en el contexto.
 * 
 * Que pasa en caso de que se termine de ejecutar un handler y la tarea tenga m�s se�ales pendientes ?
 * Deber�a ejecutar los handlers y no seguir con la ejecuci�n normal de la tarea, cosa que no sucede con esta
 * simple aproximaci�n que planteamos. Algo que se me ocurri� que si resolver�a el tema, ser�a lo siguiente:
 * ademas de poner como IP de contexto la direcci�n del handler, pongo al fondo del stack un salto a un pedazo
 * de c�digo que llame a una syscall (signal_check), que lo que haga es seguir el proceso de verificaci�n de
 * se�ales pendientes, y ejecuci�n de los handlers correspondientes.
 *
 * 8-May-2004
 * Giro de 90� en la implementaci�n base de se�ales.
 * Por cuestiones de concurrencia, y un an�lisis "profundo" del impacto que tienen recibir una se�al mientras
 * se procesa un handler de otra, decid� sobre la marcha cambiar la implementaci�n.
 * Lo que har� ser� lo siguiente: El check_sigpending ir� verificando si hay se�ales pendientes. Suponiendo que
 * haya m�s de una, lo que se har� ser� lo siguiente. Para el primer handler se pondr� su direcci�n en el EIP del
 * contexto de la tarea, y se pushear� al fondo del stack la direcci�n de un wrapper (creado por execve) el cu�l
 * me restaura los registros usados por la tarea, y luego har� un ret (para lo cual tuve que haber puesto el EIP
 * del contexto al fondo del stack). Para las dem�s se�ales pendientes que haya detectado check_sigpending, se
 * pushear� su direcci�n en el stack, con lo que primero se ejecutar� la primera se�al pendiente detectada,
 * al finalizar est� se iran sacando sistem�ticamente del stack las direcciones de los handlers de la �ltima a
 * hacia la segunda pendiente. Es decir, si al momento de schedulear tengo pendientes las se�ales 1,2,3 y 10
 * primero se ejecutar� la 1, luego la 10, la 3 y finalmente la 2.
 
 * 9-May-2004
 * La gata flora se apodera de mi :-)
 * Luego de haber implementado el sistema comentado anteriormente, me he dado cuenta que no funciona (al
 * menos no como lo implement�). Cu�l es el problema ? Todos los handlers logran ejecutarse con continuidad
 * sin que el kernel se entere. Si bien esto es altamente performante :-), no me permite ir variando
 * la m�scara de se�ales ( task->sigmask ) la cual me dice que se�ales debo ignorar mientras estoy
 * en el handler actual. Asi que, anulo esta implementaci�n y vuelvo a algo muy cercano a la implementaci�n
 * original.
 * En primer lugar: la funci�n check_sigpending recibe como par�metro un flag, el cu�l indica si debe o no
 * guardar el contexto de la tarea. Esto creo lo explique en reiteradas oportunidades, pero en pos de aumentar
 * la posibilidad de comprensi�n, lo volver� a explicar. S�lo debo guardar el contexto de la tarea en dos
 * ocasiones: 1) Si tengo que interrumpir la ejecuci�n de la tarea para ejecutar un handler. 2) Si debo
 * interrumpir la interrupci�n de un handler para ejecutar otro handler (siempre y cuando task->sigmask
 * me lo permita).
 * En que caso no debo guardar el contexto ? Supongamos que estaba ejecutando la tarea, y recibo dos se�ales:
 * guardo el contexto y ejecuto un handler. Cuando retorno de ejecutar ese handler (v�a INT sys_signal_check)
 * puedo ejecutar el otro handler sin necesidad de guardar el contexto, lo cu�l hice unos instantes antes.
 * A continuaci�n se detalla el wrapper que se ejcuta una vez terminado el handler (el cu�l se carga en la
 * llamada execve):
 *	*(ptr_exit+12) = 0xb8; 	// Codigo de operacion: "mov eax, "
 *  *(unsigned long *)(ptr_exit+13) = SYS_SIGNALS | SYS_SIGNAL_CHECK; 
 *  *(ptr_exit+17) = 0xcd;	// int
 *  *(ptr_exit+18) = 0x50;	// 0x50
 *  *(ptr_exit+19) = 0x5e;	// pop esi	// Con estos popeos recupero los registros de prop�sito general
 *  *(ptr_exit+20) = 0x5f;	// pop edi  // que fueron pusheados antes de ejecutar un handler de se�al
 *  *(ptr_exit+21) = 0x5a;	// pop edx
 *  *(ptr_exit+22) = 0x59;	// pop ecx
 *  *(ptr_exit+23) = 0x5b;	// pop ebx
 *  *(ptr_exit+24) = 0x58;	// pop eax
 *  *(ptr_exit+25) = 0xc3;	// retn
 *  new_task->sigcheck_addr =  GET_OFFSET(ptr_exit + 12) + mem->vdir;
 *
 * Este wrapper se ubica en el segmento de datos de la tarea, y se guarda su direcci�n en task->sigcheck_addr.
 * Esa direcci�n se pone en el stack de la tarea, como direcci�n de retorno del handler :-)
 * 
 * 12-May-2004
 * Ya tenemos la base del sistema de posteo y entrega (post & deliver) de se�ales. Es el momento de hablar de
 * la implementaci�n de las m�scaras de se�al. Vamos a tener dos tipos de m�scaras: la primera seteada 
 * mediante la llamada al sistema sigprocmask, la cu�l indica a que se�ales debemos inhibir mientras ejecutamos
 * c�digo de la tarea; mientras que el segundo tipo ser� la m�scara definida mediante la llamada sigaction la
 * cu�l nos dice que se�ales debo "ignorar" mientras ejecuto el handler de una se�al en particular. Vale aclarar
 * que el t�rmino "ignorar" no es del todo correcto, ya que seg�n POSIX, cuando se IGNORA una se�al, se la
 * descarta (definiendo como handler de la se�al a SIG_IGN). Que pasa entonces cuando yo pongo una m�scara de 
 * se�al que inhiba a la se�al SIGCHLD, por ejemplo ? Cuando se reciba esa se�al, se la va a marcar como 
 * pendiente (post), pero su handler se ejecutar� (deliver) reci�n cuando se "desinhiba" de la m�scara.
 *
 * A continuaci�n voy a intentar dar una idea de como ser� la implementaci�n. La forma real de entenderla es
 * estudiando el c�digo (y s�, no hay otra). Se ejecuta check_sigpending, para ver si hay se�ales
 * pendientes, cuando se encuentra alguna, antes de preparar todo para ejecutar el handler, verifico si esa
 * se�al est� inhibida mediante la m�scara sigmask (que se encuentra en el task_struct de la tarea), usando la
 * macro IS_MASKED. Obviamente la se�al no se ejecuta mientras se encuentre inhibida.
 * Ese valor sigmask, de donde sale ? Como ya aclar� anteriormente, puede ser seteado con la llamada sigprocmask
 * o bien, es el valor sa_mask de la estructura sigaction, la cual se se define mediante la llamada sigaction.
 * En esta primera aproximaci�n, haremos lo siguiente. Cuando vamos a ejecutar un handler, pondremos como 
 * m�scara (es decir en sigmask) al sa_mask de ese handler, y resguardaremos el valor que pose�a sigmask en el
 * mismo stack que vinimos manipulando (Si si, otra vez a meter mando en el stack...). Cuando se termine
 * de ejecutar el handler, popearemos el valor del stack y lo regresaremos a sigmask. Para lograr esto
 * tuvimos que modificar el c�digo de sys_signal_check (sys_signal.c) y el de exec_sigpending (signal.c).
 * Sencillamente pusheamos el valor de la m�scara actual antes de pushear "signo" (el cu�l es el par�metro que
 * recibe el handler de se�al). Luego, al retornar de la ejecuci�n del handler, simplemente se popea la m�scara.
 * 
 * Realmente no mire como implementaron esto otros desarrolladores, pero me imagino que debe ser de una manera
 * terriblemente similar. Me imagino esto ya que, al realizar testeos, encuentro que si utilizo sigprocmask
 * desde un handler de se�al, al salir del handler, esa nueva m�scara que coloque la pierdo, es decir, se 
 * sobreescribe con la que se recupera del stack. Al ir a POSIX (particularmente sigprocmask) encuentro algo como
 * esto: "When a thread's signal mask is changed in a signal-catching function that is installed by sigaction()
 * the restoration of the signal mask on return from the signal-catching function overrides that change
 * (see sigaction()).", lo que me indica que probablemente hallan almacenado las m�scaras dentro de una pila.
 *
 * 22-May-2004
 * Es realmente problematico que no se encolen las se�ales, ya que si un proceso crea 10 hijos, y todos
 * ellos terminan simultaneamente, varios quedar�n en Zombies, ya que hay solo 1 bit disponible en
 * task->sigpending. 
 * Se me ocurri� como forma f�cil de solucionar esto, colocar en la estructura task_signal un entero
 * por cada se�al, en el cu�l podemos almacenar la cantidad de se�ales de un tipo que hay. Cada vez que
 * ejecutamos un handler, decrementamos ese valor. De este modo, no perdemos se�ales.
 *
 */







// Variables que contienen cuantos malloc y cuantos frees realiz� el kernel para alocar estructuras de
// se�ales (s�lo con prop�sitos de debug)
unsigned int task_signals_alloc = 0;
unsigned int task_signals_free = 0;



/*!	\brief Verifica si hay se�ales pendientes
 *	\param task tarea en la cu�l va a verificarlas
 *	\param save_context flag que indica si debe guardarse el contexto de la tarea
 *  \return 0 si no pendientes, 1 si las hay y -1 en caso de error
 */
int check_sigpending(task_struct_t *task, int save_context)
{
	// No verificar ning�n tipo de se�ales para init (al menos por ahora)
	if (actual==pre_init_task)
		return -1;

	if (getvar("pepe")==1)
		return 0;

	// Si no hay se�ales pendientes... me voy
	if (TASK_SIGPENDING(task)==0)	{
		return 0;
	}

	int signo = 0;
	unsigned long sig = TASK_SIGPENDING(task);

	while (signo < SIGMAX) {
		// Si la se�al est� pendiente y no est� inhibida (o enmascarada) ...
		if ( (sig & 1) && !IS_MASKED(task,signo)) {
			exec_sigpending(task, signo, save_context);
			TASK_SIGNAL_COUNT(task, signo)--;
			if (TASK_SIGNAL_COUNT(task, signo)<1)
				// Borro el bit que indica que la se�al est� pendiente
				TASK_SIGPENDING(task) = TASK_SIGPENDING(task) ^ (1<<signo);
			// Me voy avisando que encontre al menos una se�al pendiente
			return 1;
		}
		// No importa la m�scara para SIGKILL
		else if ( (sig & 1) && ((signo==SIGKILL) || (signo==SIGSTOP)) )
			sig_kill(actual);
		sig = sig >> 1;
		signo++;
	}

	return 0;

/*
	// Barrer todas las posibles se�ales
	for (signo=0 ; (sig & 1)==0 ; signo++)
		sig = sig >> 1;

	exec_sigpending(task, signo, save_context);
	task->sigpending = task->sigpending ^ (1<<signo);

	// Si llegu� al final y no hay m�s pendientes
	if (signo > SIGMAX) {
		kprintf("No hay m�s pendientes ???\n");
		return 0;
	}


	// Retorno al scheduler, diciendo que hab�a la menos una se�al pendiente
	return 1;
*/
}



/*!	\brief Prepara a la tarea para ejecutar un handler
 *	\param task tarea a preparar
 *	\param signo se�al de la cu�l se preparar� a la tarea para ejecuci�n de su handler
 *	\param save_context flag que indica si debe guardarse el contexto de la tarea
 *	\todo verificar el tema de SIG_IGN, y en que momento debe considerarse
 *	\todo Evaluar sa_flags de la estructura sigaction 
 */
void exec_sigpending(task_struct_t *task, int signo, int save_context)
{
	kprintf("exec_sigpending(): entrando\n");
	
	if (TASK_SIGNAL_HANDLER(task, signo) == SIG_DFL) {
		exec_sig_default(task, signo);
		return;
	}
	
	// Debo inhibir la posibilidad que me conmute la tarea, ya que si lo hace con el stack desbalanceado
	// puede pasar cualquier cosa
	cli();
	
	// Obtener el contexto de la tarea
	struct int_regs_ext *context = GET_CONTEXT(task);

	unsigned long *user_ptr = (unsigned long *) context->esp;
	// Obtener la direcci�n f�sica del stack de la tarea
	user_ptr = convertir_direccion( user_ptr, actual->cr3_backup);

	// S�lo debo "pushear" el EIP de la tarea y los registros para la primer se�al pendiente
	// verificando con first_signal
	if (save_context == 1) {
//		kprintf("EXEC: signo first: %d\n", signo);
		*(--user_ptr) = context->eip;
//		kprintf("Pusheando al stack el EIP: 0x%x\n", context->eip);
		context->esp -= 4;
		*(--user_ptr) = context->eax;
		*(--user_ptr) = context->ebx;
		*(--user_ptr) = context->ecx;
		*(--user_ptr) = context->edx;
		*(--user_ptr) = context->edi;
		*(--user_ptr) = context->esi;
		context->esp -= (6 * sizeof(dword));
		
		*(--user_ptr) = TASK_SIGMASK(task);
		context->esp -= 4;
//		kprintf("Pusheando MASCARA: 0x%x\n", task->sigmask);
		
		
		// Paso como argumento el n�mero de se�al
		*(--user_ptr) = signo;
		// Y como direcci�n de retorno el wrapper "sigcheck" que popea los registros (EAX - ESI)
		*(--user_ptr) = TASK_SIGADDR(task);
		context->esp -= 8;
		context->eip = (dword) TASK_SIGNAL_HANDLER(task, signo);
	}
	else {
		// Paso como argumento el n�mero de se�al (no muevo el stack porque quedo 4 bytes desplazado)
		*(user_ptr) = TASK_SIGMASK(task);
//		kprintf("Pusheando MASCARA: 0x%x\n", task->sigmask);
		context->esp -= 4;
		*(--user_ptr) = signo;
		*(--user_ptr) = TASK_SIGADDR(task);
		// Y pusheo en el stack la direcci�n del handler (cuando termine el otro handler, se ejecutar� este)
		context->eip = (dword) TASK_SIGNAL_HANDLER(task, signo);
		context->esp -= 4;
		//kprintf("EXEC: signo no first: %d\n", signo);
	}
	
	// Coloco como m�scara, la m�scara del proceso (seteada con sigaction)
	TASK_SIGMASK(task) = TASK_SIGNAL_MASK(task, signo);
	sti();

}

/*!	\brief Ejecuta el handler por default para una se�al
 *  \todo momentaneamente solo se soporta el SIG_DFL para SIGKILL. Las dem�s se�ales solo
 * 		  imprimen una leyenda...
 */
void exec_sig_default (task_struct_t *task, int signo)
{
	if (signo==9)
		sig_kill(actual);
	else kprintf("Proceso PID: %d\tSIG_DFL para la senal: %d no implementado \n", task->pid, signo);
}


void sig_kill(task_struct_t *task)
{
	sys_exit(0);
}

