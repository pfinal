/* tarea.c */
#include "stdarg.h"
#include <sys/types.h>
#include <lib/routix.h>
#include <routix.h>
#include <string.h>
#include <signal.h>

void catchear_hijos(int signo);

int hijos;

int main (void) 
{

	int pid;
	int count;

	if ( (count=hijos=getvar("numhijos")) == -1 ) {
		count=hijos=5;
	}



	signal(SIGCHLD, catchear_hijos);

	while ( count-- ) {

		if ( (pid=fork()) == -1 ) {
			perror("error al forkear");
			exit(1);
		}
		
		// hijo
		else if ( ! pid ) {
			
			if ( (count=getvar("numlecturas")) == -1 ) {
				count=5;
			}
			
			while ( count--) {
				read_debug(5);
				//exec("print.bin");
			}

			exit(0);
		}

	}

	while ( hijos ) ;

	printf("todos los hijos terminaron correctamente\n");
	
	exit(0);
  
}




void catchear_hijos(int signo)
{
	if (signo!=SIGCHLD)	{
		printf("DEBERIA HABER RECIBIDO SIGCHLD y recibi: %d\n", signo);
		exit(-1);
	}
	
	int value;

	hijos--;
	wait(&value);

}

