/* tarea.c */
#include "stdarg.h"
#include "routix.h"
#include "string.h"

char argumento1[] = "Argumento-1";
char argumento2[] = "Argumento-2";
char argumento3[] = "Argumento-3";
char argumento4[] = "Argumento-4";

char entorno1[] = "USERNAME=root";
char entorno2[] = "SHELL=shell.bin";
char entorno3[] = "LIB=/usr/lib";
char entorno4[] = "INCLUDE=/usr/include";


int main (void)
{
	char *argv[6];
	argv[0] = argumento1;
	argv[1] = argumento2;
	argv[2] = argumento3;
	argv[3] = argumento4;
	argv[4] = NULL;

	char *envp[6];
	envp[0] = entorno1;
	envp[1] = entorno2;
	envp[2] = entorno3;
	envp[3] = entorno4;
	envp[4] = NULL;

	execve("arg2.bin", argv, envp);
}
