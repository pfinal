/* tarea.c */
#include "stdarg.h"
#include "routix.h"
#include "string.h"

int main (int argc, char **argv, char **envp)
{
	printf("Recib� %d argumentos\n", argc);
	printf("Ellos son:\n");
	int i;
	for ( i=0 ; argv[i] && i<argc ; i++)
		printf("Argumento %d: *%s*\n", i, argv[i]);

	for ( i=0 ; envp[i] && envp ; i++)
		printf("Entorno %d: *%s*\n", i, envp[i]);

}
