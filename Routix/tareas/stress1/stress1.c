// stress2.bin
// Ejecuta la tarea stack.bin mcuhas veces, en busca de memory leeks 
// o colgadas
#include <stdarg.h>
#include <routix.h>
#include <sys/types.h>
#include <lib/routix.h>
#include <signal.h>

char msg[40];

#define DEMORA	0xaffff

pid_t pides[4];
void child_handler (int signo);
void usr_handler (int signo);

int num_sigusr = 0;

void main(void) 
{
	int hijos = getvar("hijos");
	hijos = (hijos==-1) ? 5 : hijos;
	
	pid_t child;

	if (getvar("signal")==1) {
		printf("Habilitando un handler para SIGCHLD\n");
		if (signal(SIGCHLD, child_handler)==-1) {
			perror("signal");
			exit(-1);
		}
		if (signal(SIGUSR1, usr_handler)==-1) {
			perror("signal user");
			exit(-1);

	
		}
	}
	else printf("Sin uso de senales\n");
	int i;
	for (i=0 ; i<hijos ; i++) {
		child = fork();
		if (child==0) {
//			kill(getppid(), SIGUSR1);
//			if (getvar("sleep")==1)
				while(1) {
					if (getvar("fin")==1)
						exit(0);
					sleep(10);
				}
			exit(getpid()+100);
		}
	}

	int tiempo = getvar("tiempo");
	tiempo = (tiempo==-1) ? 5 : tiempo;

	printf("Soy el padre %d y me pongo a dormir\n", getpid());
	sleep(tiempo);
	printf("Soy el padre %d y me acabo de despertar\n", getpid());
	printf("Cantidad de SIGUSR1 recibidas: %d\n", num_sigusr);

}

void child_handler (int signo)
{
	int value;
	waitpid(0, &value, WNOHANG);
}

void usr_handler (int signo)
{
	num_sigusr++;
}
