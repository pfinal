/* tarea.c */
#include "stdarg.h"
#include <sys/types.h>
#include <lib/routix.h>
#include <routix.h>
#include <string.h>
#include <signal.h>

void handler(int);
void handler1(int signo);
long get_esp(void);

int sig = 0;
 
int main (void)
{
	kill(getppid(), SIGUSR1);
	sigset_t mascara;
	sigfillset(&mascara);
	sigdelset(&mascara, 2);
//	sigprocmask(SIG_SETMASK,&mascara ,NULL);

	sigemptyset(&mascara);
	sigaddset(&mascara, 4);
	struct sigaction sig1;
	sig1.sa_handler = handler;
	sig1.sa_mask = mascara;
	sigaction (3, &sig1, NULL);
	 
	int valor = 0xabcd;
//	signal(3, handler);
	signal(4, handler1);
	signal(2, handler1);
	printf("Valor antes de senales: 0x%x\n", valor);
	printf("Tarea.bin: Durmiendo en un while...\n");
	__asm__ __volatile__ ("mov $0x11111111,%eax ; mov $0x22222222,%ebx ; mov $0x33333333,%ecx ; mov $0x44444444,%edx ; mov $0x55555555,%edi ; mov $0x66666666,%esi");
	while(sig!=4);
	printf("Valor despues de senales: 0x%x\n", valor);
	
	return 0;
}

void handler(int signo)
{
	sig = signo;
	printf("handler 3 de tarea.bin: Se recibio la senal: %d\t PID: %d\n", signo, getpid());
	int i;
	for (i=0 ; i<0x5fffff; i++);
	printf("saliendo del handler 3\n");
	
}

void handler1(int signo)
{
	// Se puede setear una m�scara desde dentro de un handler ????
	printf("handler1 de tarea.bin: Se recibio la senal: %d\t PID: %d\n", signo, getpid());
	if (signo==2) {
		sigset_t mascara;
		sigemptyset(&mascara);
		sigprocmask(SIG_SETMASK, &mascara, NULL);
	}
	
	sig = signo;
	//_debug();
}

