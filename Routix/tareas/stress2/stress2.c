// stress2.bin
// Si no hay encolamiento de se�ales, solo la se�al SIGUSR1 del primer hijo se recive
// las otras se pierden
#include <stdarg.h>
#include <routix.h>
#include <sys/types.h>
#include <lib/routix.h>
#include <signal.h>

char msg[40];

#define DEMORA	0xaffff

pid_t pides[4];
void catchear_hijos(int);
void sig_usr1(int);

int hijos_terminados;
int hijos_creados;


void main(void) 
{
	struct sigaction senal;
	sigfillset(&senal.sa_mask);
	senal.sa_handler = sig_usr1;
	sigaction(SIGUSR1, &senal, NULL);
//	signal(SIGUSR1, sig_usr1);
	
	int i, j;
	int l = getvar("veces");
	if (l==-1) {
		printf("No existe el valor veces. Seteandolo por default a 10");
		l = 10;
	}
	sigfillset(&senal.sa_mask);
	senal.sa_handler = catchear_hijos;
	sigaction(SIGCHLD, &senal, NULL);

//	signal(SIGCHLD, catchear_hijos);
	
	for (j=1 ; j<=l ; j++) {
		
		hijos_creados = 0;
		
		if ((pides[0]=fork())==0) {
			if (execve("stress2a.bin", NULL, NULL)==-1)
				perror("exec");
			exit (-1);
		}	
		if ((pides[1]=fork())==0) {
			if (execve("stress2a.bin", NULL, NULL)==-1)
				perror("exec");
			exit (-1);
		}	
		if ((pides[2]=fork())==0) {
			if (execve("stress2a.bin", NULL, NULL)==-1)
				perror("exec");
			exit (-1);
		}	
		if ((pides[3]=fork())==0) {
			if (execve("stress2a.bin", NULL, NULL)==-1)
				perror("exec");
			exit (-1);
		}	
		printf("Esperando por info de hijos creados\n");
		while (hijos_creados < 4)
			usleep(100000);

		
		printf("\nStress info: se ha logrado ejecutar %d procesos\n", j*4);
		kill(pides[0], 4);
		kill(pides[1], 4);
		kill(pides[2], 4);
		kill(pides[3], 4);
	}

		while (l*4 > hijos_terminados);
  
}

void catchear_hijos(int signo)
{
	if (signo!=SIGCHLD)	{
		printf("DEBERIA HABER RECIBIDO SIGCHLD y recibi: %d\n", signo);
		exit(-1);
	}
	int value;
	hijos_terminados++;
	wait(&value);
}

void sig_usr1(int signo)
{
	if (signo!=SIGUSR1) {
		printf("en el handler de sigusr1 recibi: %d\n", signo);
		exit(-1000);
	}
	hijos_creados++;
	printf("SIGUSR1: un hijo Ok\n");
}
