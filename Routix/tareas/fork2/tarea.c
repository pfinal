/* tarea.c */
#include "stdarg.h"
#include <sys/types.h>
#include <lib/routix.h>
#include <string.h>

char nombre[] = "Sistema operativo Routix -0.0.0.0.1";
int valor;

int main (void)
{
	pid_t pid = fork();
	if (pid==0) {		// Hijo
		printf("\nYo soy el hijo %d y mi padre es %d\n", getpid(), getppid());
		printf("Hijo: *%s*\n", nombre);
	}
	else if (pid==-1) {
		perror("fork");
		exit(-1);
	}
	else {
		printf("\nYo soy el padre %d y mi hijo es: %d\n", getpid(), pid);
		printf("Padre: *%s*\n", nombre);
		pid = wait(&valor);
		printf("\nWait: mi hijo %d retorno: %d\n", pid, valor);
	}
}
