/* tarea.c */
#include "stdarg.h"
#include "routix.h"
#include "string.h"

int main (void)
{
	printf("Ejemplo utilizado para verificar un PAGE-fault por falta de privilegios\n");

	long *p = 0;
	printf("Intentando leer la direccion: 0x%x\n", p);
	printf("Contenido de la posicion 0x%x: 0x%x\n", p, *p);
	printf("Voy a intentar escribir la direccion: 0x%x\n", p);
	*p = 0xabcdef01;
	printf("ok...\n");
	printf("Puede escribir en la direccion: 0x%x el valor: 0x%x\n", p,*p);
	
	
	
	exit(0);
}
