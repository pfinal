/* tarea.c */
#include "routix.h"
#include "alloc.h"

int buff[50];
char str[]="Mi nombre es martin  ";

#define VECES	10
#define SIZE	100

void main(void) 
{
	int veces, size;
	int mem_before, mem_after;
	
	mem_before = free_mem();
	
	veces = getvar("veces");
	size = getvar("size");

	if (size==-1 || size>(4096-50))
		size = 100;

	if (veces==-1)
		veces = 10;

	printf("Utilizando veces: %d con size: %d\n", veces, size);
	
	int i;
	void *p, *q;
	for (i=0 ; i<veces/2 ;i++) {
		p = malloc(size);
		q = malloc(size);
		free (p);
		free (q);
	}
	
	mem_after = free_mem();
	printf("Paginas libres antes: %d\tahora: %d\n", mem_before, mem_after);
	show(1);
}
