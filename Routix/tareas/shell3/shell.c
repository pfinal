/* tarea.c */
#include "stdarg.h"
#include <sys/types.h>
#include <lib/routix.h>
#include <routix.h>
#include <string.h>
#include <signal.h>

char msg[50];

char msg1[50];
char msg2[50];

char str[]="Shell v 0.0.0.3";

int j, k;
int pid;
int ntasks = 0;		// Tareas hijas del shell (levantar condici�n de salida)
int aux;

void _waiting_childs(int signo);

void main(int argc, char **argv) 
{
	printf("Shell3: Cantidad de argumentos recibidos de init: %d\n", argc);
	int bucle;
	for (bucle = 0; bucle < argc ; bucle++) 
		printf("Argumento %d: %s\n", bucle, argv[bucle]);
	
	
    puts(str);

    putchar('\n');

	signal(SIGCHLD, _waiting_childs);

    while(1) {

	printf("kernel# ");
	gets(msg);

	if ( ! strcmp(msg, "clear") ) {
	    clrscr();
	}
	else if ( ! strcmp(msg, "exec") ) {
	    printf("Ingrese nombre de tarea:");
	    gets(msg1);
		pid = fork();
		if (pid==-1)
			perror("fork");
		else if (pid==0) {		//Shell HIJO
			if (execve(msg1, NULL, NULL)==-1)
				perror("No pudo ejecutarse");
			exit(-1);
		}
		ntasks++;
	}
	else if ( ! strcmp(msg, "echo") ) {
	    printf("Ingrese texto:");
	    gets(msg1);
	    printf("%s\n", msg1);
	}
	else if ( ! strcmp(msg, "\n") ) {
	}
	else if ( ! strcmp(msg, "ps") ) {  proc_dump(); }
	
	else if ( ! strcmp(msg, "ps count") ) {  proc_dump(); }

	else if ( ! strcmp(msg, "timers") ) {  timer_dump(); }

	else if ( ! strcmp(msg, "read") ) {  
	
	  puts("Sector: ");
	  gets(msg1);

	  read_debug(atoi(msg1));
	
	}

	else if ( ! strcmp(msg, "kill") ) {

          puts("Pid: ");
	  gets(msg1);
      puts("Senal: ");
	  gets(msg2);
	  if (kill(atoi(msg1), atoi(msg2))==-1)
	  	perror("kill");
		
	}

	else if ( ! strcmp(msg, "info") ) {
	    printf("Ingrese el PID: ");
	    gets(msg2);
	    if (proc_dump_v(atoi(msg2))==-1)
		perror("proc dump verbose"); 
	}

	else if ( ! strcmp(msg, "renice") ) {
	    printf("Ingrese el PID: ");
	    gets(msg1);
	    printf("Ingrese la nueva prioridad: ");
	    gets(msg2);
	    if (renice( atoi(msg1), atoi(msg2)) == -1)
			perror("renice");

		proc_dump();
	}
	
	else if ( ! strcmp(msg, "free mem") ) {  printf("Paginas disponibles: %d\n",free_mem() ) ; }
	
	else if (! strcmp(msg, "show morecores")) {
		show(1);
	}

	else if (! strcmp(msg, "show cache")) {
		show(2);
	}

	else if (! strcmp(msg, "show zombies")) {
		show(3);
	}

	else if (! strcmp(msg, "test malloc")) {
		show(4);
	}

	
	else if (! strcmp(msg, "show ntasks")) {
		printf("Cantidad de hijos del shell: %d\n", ntasks);
	}

	else if (! strcmp(msg, "clear alloc")) {
		show(5);
	}

	else if (! strcmp(msg, "show alloc")) {
		show(6);
	}

	else if (! strcmp(msg, "clear signals alloc")) {
		show(7);
	}
	else if (! strcmp(msg, "show signals alloc")) {
		show(8);
	}
	else if (! strcmp(msg, "show wrapper")) {
		show(9);
	}
	else if (! strcmp(msg, "clear wrapper")) {
		show(10);
	}

	
	else if (! strcmp(msg, "setvar")) {
	    printf("Ingrese el nombre: ");
	    gets(msg1);
	    printf("Ingrese el valor: ");
	    gets(msg2);
		if (setvar(msg1, atoi(msg2))==-1)
			printf("No pueden definirse m�s variables\n");
	}

	else if (! strcmp(msg, "getvar")) {
	    printf("Ingrese el nombre: ");
	    gets(msg1);
		j = getvar(msg1);
		if (j==-1)
			printf("%s no definida\n", msg1);
		else printf ("Valor de %s: %d\n", msg1, j);
	}

	else printf("comando o nombre de archivo erroneo\n");
	// Debo hacerme cargo de los hijos que van terminando
	/*
	if (ntasks>0) {
		pid = waitpid (0, &aux, WNOHANG);
		if (pid>0) {
			printf("SHELL 2 - Proceso: %d termino con: %d\n", pid, aux );
			ntasks--;
		}
			
	}
	*/
  }
	
}    

void _waiting_childs(int signo)
{
	pid_t pid = waitpid (0, &aux, WNOHANG);
	if (pid>0) {
		printf("SHELL - Recibida SIGCHLD - Proceso %d termino con: %d\n", pid, aux );
		ntasks--;
	}
	
}
