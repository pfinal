/* INIT */
#include "stdarg.h"
#include <sys/types.h>
#include <lib/routix.h>
#include <routix.h>
#include <string.h>
#include <signal.h>
#include <sched.h>

void sig_chld(int);

char *args[] = {"Init", NULL};

int main (void)
{
	sigset_t mascara;
	// Init no puede ejecutar otros handlers mientras hace un waitpid... sino, puede corromperse
	// el stack
	sigfillset(&mascara);
	struct sigaction senal;
	senal.sa_handler = sig_chld;
	senal.sa_mask = mascara;
	printf("Iniciando Init.bin\n");
	if (sigaction(SIGCHLD, &senal, NULL)==-1) {
		perror("sigaction");
	}
	pid_t pid = fork();
	if (pid==-1) {
		perror("init.bin");
		exit(-1);
	}
	else if (pid==0) {
		if (execve("shell.bin", args, NULL)==-1)
			perror("shell.bin");
	}
	else {
		while(1)
			sched_yield();
	}
}

void sig_chld(int signo)
{
	pid_t pid;
	int value;
	pid = waitpid(-1, &value, WNOHANG);
	printf("Init catcheo la finalizacion de %d con el valor %d\n", pid, value);
}
