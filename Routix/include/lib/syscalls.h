/* SysCalls.h */
/*
*/
/*
// Funciones de libreria
int sys_putc (char);

// Grupo Console
int sys_gets (char *str);
int sys_print (void *buff, size_t largo);

// Grupo process
int sys_void (void);
int sys_exec (char *nombre);
void sys_show(int);

// Grupo Timer
int sys_sleep(int segundos);
int sys_usleep(int usegundos);
int sys_proc_dump(int segundos);
int sys_kill(int pid, int sig);

// Grupo Misc
int setvar (char *nombre, int valor);
int getvar (char *nombre);
*/