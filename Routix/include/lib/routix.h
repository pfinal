#ifndef __ROUTIX_TYPES
#include <sys/types.h>
#endif

int clrscr(void);
void gets (char *str);
void voido (void);
int putchar (char car);
void puts(char *str);
void printf ( char *string, ...);
int sprintf(char *str, const char *string, ...);
void sputchar (char car);

int sleep(int);
int proc_dump(void);
void show(int);
int renice(word pid, word prioridad);
int kill( pid_t pid, int sig);

size_t free_mem(void);
int sleep(int);
int proc_dump(void);
int proc_dump_v(int);


int setvar(char *nombre, int valor);
int getvar(char *nombre);
void perror (char *str);
int execve (char *tarea, char **argv, char **envp);
int exec (char *);


void exit(int);
pid_t fork();
int clone(void *func);
#define WNOHANG 1
pid_t waitpid (pid_t pid, int *valor, int options);
pid_t wait (int *valor);

pid_t getpid(void);
pid_t getppid(void);

int kill(pid_t pid, int sig);


#define _debug()	__asm__ __volatile__ ("push %eax; 		\
											mov $1, %eax;	\
									debug:	cmp $1, %eax;	\
											je debug;		\
											pop %eax");


//#endif
