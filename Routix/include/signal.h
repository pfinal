/*! \brief Definiciones de se�ales comunes a Kernel y User Mode
 *  \note Primera aproximaci�n a definicions de se�ales tipo SYS V
 */

#include "sys/types.h"


#ifndef __SIGNAL
#define __SIGNAL

typedef unsigned long sigset_t;
typedef void (*__sighandler_t) (int);


//void (*signal(int sig, void (*func)(int)))(int);
void *signal (int sig, void (*func)() );

struct sigaction {
	__sighandler_t sa_handler;
	sigset_t sa_mask;
	int sa_flags;
	void (*sa_restorer)(void);
};


#define SIGHUP		 1
#define SIGINT		 2
#define SIGQUIT		 3
#define SIGILL		 4
#define SIGTRAP		 5
#define SIGABRT		 6
#define SIGIOT		 6
#define SIGBUS		 7
#define SIGFPE		 8
#define SIGKILL		 9
#define SIGUSR1		10
#define SIGSEGV		11
#define SIGUSR2		12
#define SIGPIPE		13
#define SIGALRM		14
#define SIGTERM		15
#define SIGSTKFLT	16
#define SIGCHLD		17
#define SIGCONT		18
#define SIGSTOP		19
#define SIGTSTP		20
#define SIGTTIN		21
#define SIGTTOU		22
#define SIGMAX		23		// Numero de se�ales


#define SIG_ERR   	((void (*)(int)) -1)
#define SIG_DFL  	((void (*)(int))  0)
#define SIG_IGN   	((void (*)(int))  1)
#define SIG_HOLD   	((void (*)(int))  2)

int sigprocmask(int flag, const sigset_t *set, sigset_t *old_set);

#define	SIG_BLOCK   	0
#define	SIG_UNBLOCK 	1
#define SIG_SETMASK 	2

#define sigemptyset(set)		(*set=0)
#define sigfillset(set)			( (*set=0xffffffff) ? 0 : -1)
#define sigaddset(set, signo)	( (signo<SIGMAX) ? ( *set |= (1<<signo))==0 : -1  )
#define sigdelset(set, signo)	( (signo<SIGMAX) ? ( *set&=(~(1<<signo)))&0  : (unsigned long) -1)
#define sigismember(set, signo) ( (signo<SIGMAX) ? ((*set>>signo)&1) : -1  )


int sigaction (int signo, struct sigaction *act, struct sigaction *oact);

#endif
