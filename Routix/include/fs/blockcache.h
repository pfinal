/* block.h */

#ifndef __BLOCK_CACHE
#define __BLOCK_CACHE

#include <routix/device.h>
#include <routix/task.h>
#include <routix/system.h>
#include <sys/list.h>

// Cache
typedef enum block_state { READ_LOCKED, WRITE_LOCKED, SYNCRONIZING, UNLOCKED, DIRTY } block_state;

typedef struct block_cache_t {
	device_t device;
	word sector;
	byte *buffer;

	// Estado del bloque
  	block_state state;
	unsigned int lock_count;														/// \var cantidad de locks sobre el bloque

	// Info para el driver
	operation_t operation;
	int ret;

	LIST_NEW(task_struct_t)		pending_process_list;			// lista de procesos en espera por este bloque
	LIST_DATA(block_cache_t)	free;											// puede pertenecer a una lista de bloques libres
	LIST_DATA(block_cache_t)	cached;										// puede pertenecer a una lista contenida en un hash
	LIST_DATA(block_cache_t)	driver_request_list;			// puede pertenecer a una lista de requests a un driver

} block_cache_t;

#define BLOCK_CACHE_DEVICE(bf)			((bf)->device)
#define BLOCK_CACHE_SECTOR(bf)			((bf)->sector)
#define BLOCK_CACHE_BUFFER(bf)			((bf)->buffer)
#define BLOCK_CACHE_STATE(bf)			((bf)->state)
#define BLOCK_CACHE_LOCKCOUNT(bf)		((bf)->lock_count)
#define BLOCK_CACHE_OPERATION(bf)		((bf)->operation)
#define BLOCK_CACHE_RETURN(bf)			((bf)->ret)
#define BLOCK_CACHE_PENDING_PROCESS_LIST(bf)		((bf)->pending_process_list)

#define DRIVER_REQUEST_DEVICE(bf)		((bf)->device)
#define DRIVER_REQUEST_SECTOR(bf)		((bf)->sector)
#define DRIVER_REQUEST_BUFFER(bf)		((bf)->buffer)
#define DRIVER_REQUEST_STATE(bf)		((bf)->state)
#define DRIVER_REQUEST_OPERATION(bf)	((bf)->operation)
#define DRIVER_REQUEST_RETURN(bf)		((bf)->ret)

// Definiciones
#define BLOCK_SIZE	512


// Funciones
void start_block_cache(int totalmemory);

block_cache_t *getrequest(device_t device);
inline void sendrequest(block_cache_t *block);
inline void endrequest(block_cache_t *block);
int cache_read(device_t device, word sector, unsigned int start, char *dstbuffer, unsigned int len);

// Funciones locales de debug
void show_cached_list(void);


#define CACHE_READ(device,sector,buffer)	( cache_read(device,sector,0,buffer,BLOCK_SIZE) )


#endif
