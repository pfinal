//! Definicion de tipos

#ifndef __ROUTIX_TYPES
#define __ROUTIX_TYPES

#define NULL	0

#define TRUE	1
#define FALSE	0

#define OK		0


typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned long long qword;

typedef dword addr_t;
typedef long size_t;
typedef unsigned long ssize_t;

typedef short int pid_t;

typedef unsigned long long int ticks_t;

#endif
