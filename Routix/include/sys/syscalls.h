/*! \brief Aqu� se define la ubicaci�n de las llamadas dentro de los vectores
 *! \note este header DEBE ser utilizado por el kernel y por la librer�a estandar de Routix
 */

#ifndef _SYSCALLS_H
#define _SYSCALLS_H


#define _syscall0(numero,retorno) __asm__ __volatile__ ("int $0x50" : "=a" (retorno) : "a" (numero))
#define _syscall1(numero,retorno,param1) __asm__ __volatile__ ("int $0x50" : "=a" (retorno) : "a" (numero), "b" (param1))
#define _syscall2(numero,retorno,param1,param2) __asm__ __volatile__ ("int $0x50" : "=a" (retorno) : "a" (numero), \
	"b" (param1), "c" (param2))
#define _syscall3(numero,retorno,param1,param2,param3) __asm__ __volatile__ ("int $0x50" : "=a" (retorno) : "a" (numero), \
	"b" (param1), "c" (param2), "d" (param3))


#define MAX_SYSCALLS	15	

// Definiciones de grupos
#define SYS_PROCESS	0
#define SYS_CONSOLE	(1 << 16)
#define SYS_SIGNALS	(2 << 16)
#define SYS_TIMER	(3 << 16)
#define SYS_MEM		(4 << 16)
#define SYS_MISC	(5 << 16)

//El valor SYS_%_MAX determina cual es el n�mero de llamadas al sistema para ese grupo, evitando problemas
// % equivale al nombre del grupo

// Funciones de grupo PROCESS
#define SYS_EXEC		0
#define SYS_VOID		1
#define SYS_FORK		2
#define SYS_PERROR		3
#define SYS_RENICE		4
#define SYS_GETPID		5
#define SYS_GETPPID		6
#define SYS_EXIT		7
#define SYS_SHOW		8
#define SYS_WAIT		9
#define SYS_EXECVE		10
#define SYS_CLONE		11
#define SYS_PROCESS_MAX	12		// Cantidad de llamadas en el grupo PROCESS. Debe ser siempre el ultimo valor

// Funciones de grupo CONSOLE
#define SYS_PRINT		0
#define SYS_GETS		1
#define SYS_CLRSCR		2
#define SYS_CONSOLE_MAX	3		// Cantidad de llamadas en el grupo CONSOLE. Debe ser siempre el ultimo valor

// Funciones del grupo SIGNALS
#define SYS_KILL			0		//
#define SYS_SIGNAL			1
#define SYS_SIGACTION		2
#define SYS_SIGNAL_CHECK	3
#define SYS_SIGPROCMASK		4
#define SYS_SIGNALS_MAX		5		// Cantidad de llamadas en el grupo SIGNALS. Debe ser siempre el ultimo valor


// Funciones de grupo TIMER
#define SYS_SLEEP		0
#define SYS_PROC_DUMP	1
#define SYS_KILLL		2		//KILL no va aqu� (debo crear un grupo SYS_SIGNAL o lo ubico en SYS_MEM ?)
#define SYS_USLEEP		3
#define SYS_PROC_DUMP_V	4
#define SYS_TIMER_DUMP	5
#define SYS_TIMER_MAX	6	// Cantidad de llamadas en el grupo TIMER. Debe ser siempre el ultimo valor

// Funciones de grupo MEM
#define SYS_MALLOC_PAGE	0
#define SYS_FREE_PAGE	1
#define SYS_FREE_MEM	2
#define SYS_MEM_MAX		3		// Cantidad de llamadas en el grupo MEM. Debe ser siempre el ultimo valor

// Funciones de grupo MISC
#define SYS_SETVAR		0
#define SYS_GETVAR		1
#define SYS_READ_DEBUG	2	
#define SYS_MISC_MAX	3		// Cantidad de llamadas en el grupo MEM. 

void syscall (void);
int sys_no_existe (dword numero);

#endif
