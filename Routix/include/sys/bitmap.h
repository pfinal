//! Definicion de tipos

#ifndef __BITMAP
#define __BITMAP

/** \brief Crea un mapa de bits con tama�o para "bits" cantidad de bits
 *	\param name nombre del mapa de bits
 *	\param bits cantidad de bits que poseer� el mapa
*/
#define BITMAP(name,bits)			unsigned long name[(bits/(sizeof(unsigned long)*8))+1]

/** \brief Inicializa a 0 el mapa de bits (es mandatorio ejecutar esta funci�n antes de utilizarlo)
 *	\param name nombre del mapa de bits
 *	\param bits cantidad de bits que poseer� el mapa
*/
#define BITMAP_INIT(name,bits)		memset(&name[0], 0, (bits/(sizeof(unsigned long)*8))+1)

/** \brief obtiene el valor de un bit
 *	\param name nombre del mapa de bits
 *	\param bit numero de bit que desea obtenerse
*/
#define BITMAP_GET_BIT(name,bit)	(((name[bit/sizeof(unsigned long)]) >> (bit%sizeof(long))) & 1)

/** \brief setea el valor de un bit dentro del mapa de bits
 *	\param name nombre del mapa de bits
 *	\param bit numero de bit que desea asignarse
 *	\param value valor que desea asignarse al bit
*/
#define BITMAP_SET_BIT(name,bit,value) {													\
										unsigned long __aux = 1;							\
										__aux = __aux << (bit%sizeof(long));				\
										if (value==0) {										\
											__aux = ~ __aux;								\
											name[bit/sizeof(unsigned long)] &= __aux;		\
										} 													\
										else {												\
											name[bit/sizeof(unsigned long)] |= __aux;		\
										}													\
										}



/*!	\brief	devuelve el primer bit disponible del bitmap que concuerde con el valor value
 *	\param	name	nombre del bitmap
 *	\param	bits	tama�o del bitmap
 *	\param	value	valor del cu�l se busca concidencia
 *	\return	n�mero de bit disponible en caso de �xito o -1 si todos est�n utilizados
 */
static inline int BITMAP_GET_FIRST_AVAILABLE(unsigned long *name,long bits,int value) {
	int _i;
	for (_i = 0 ; _i<bits ; _i++) {
		if ( BITMAP_GET_BIT(name, _i)==value )
			return _i;
	}
	return -1;
}
		
		
#endif	//__BITMAP
