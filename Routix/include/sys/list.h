/*! \addtogroup Misc
	\page list.h
	Macros para simplificar el manejo de listas
*/

#ifndef __LIST
#define __LIST

/*! a incluir en las estructuras de datos a colocar en listas
 * ej:
 *		typedef struct task_struct_t {
 * 		word pid;
 *			int priority;
 * 		....
 *			LIST_DATA(task_struct_t) runqueue;
 *			LIST_DATA(task_struct_t) zombiequeue;
 *			....
 *		} task_struct_t;
 */

//! Aloca un nodo de la lista
#define ALLOCATE(type)	( (type *) malloc(sizeof(type)) )

//! Define un puntero y aloca un nodo de la lista
#define DEF_ALLOC(type, name)	\
	type *name;	\
	( name = (type *) malloc(sizeof(type)) );


/*!	\def LIST_DATA(type)
		\brief Macro para permitir la inclusi�n de una estructura en una lista doblemente enlazada

		Al colocarlo en una estructura permite la inclusi�n del nodo como parte de una lista doblemente enlazada,
	 	veamos un ejemplo:

		\code
		typedef struct {
			pid_t pid;
			pid_t ppid;

			...

			LIST_DATA(task_struct_t) runqueue;
			LIST_DATA(task_struct_t) io_pending;
		
		} task_struct_t;

		\endcode

		el fragmento anterior muestra como las instancias de task_struct_t pueden formar parte de dos listas.
*/
#define LIST_DATA(type)		struct { struct type *next,*previous; }

// Macros de uso interno
#define LIST_NEXT(list,node) 		((node->list).next)
#define LIST_PREVIOUS(list,node) 	((node->list).previous)

#define HEADER_NEXT(header) 		((header).next)
#define HEADER_PREVIOUS(header) 	((header).previous)


/*! Creacion de la lista
 * Define la estructura que funciona como nodo HEAD
 * ej:
 *		LIST_NEW(task_struct_t) runqueue;
 */
#define LIST_NEW(type)		\
	struct {				\
		type *next;			\
		type *previous;		\
	}

#define LIST_INITIALIZER	{NULL,NULL}

#define LIST_INIT(header)			\
	HEADER_NEXT(header)=NULL;		\
	HEADER_PREVIOUS(header)=NULL;


/*! \brief Devuelve el primero nodo de la lista
 *  \note ej.: first_node = LIST_FIRST(free_list);
 */
#define LIST_FIRST(header)		\
	HEADER_NEXT(header)

/*! \brief Devuelve el �ltimo nodo de la lista
 */
#define LIST_TAIL(header)		\
	HEADER_PREVIOUS(header)



/*! \brief Verifica si una lista est� vac�a
 *  \param header header de la lista
 *  \return 1 en caso de que est� vac�a y 0 en caso contrario
 */
#define LIST_EMPTY(header)		\
	((HEADER_NEXT(header) && HEADER_PREVIOUS(header)) ? 0 : 1)


/*! Agrega un nodo al principio de la lista (entre el
 * HEAD y el primero nodo que existe). Se supone que
 * el puntero al nodo que recibe como argumento apunta
 * a una estructura prealocada.
 * ej:
 *		LIST_ADD(runqueue,nodo1);
 */
#define LIST_ADD(header,list,node)							\
	LIST_NEXT(list,node)=HEADER_NEXT(header);					\
	if ( HEADER_NEXT(header) != NULL)						\
		LIST_PREVIOUS( list, LIST_NEXT(list,node) )= node;		\
	else									\
		HEADER_PREVIOUS(header)=node;					\
	LIST_PREVIOUS(list,node)=NULL;						\
	HEADER_NEXT(header)=node;



/*! Idem a LIST_ADD pero el nuevo nodo es insertado
 * al final de la lista.
 * ej:
 *		LIST_ADD_TAIL(runqueue,nodo2);
 */
#define LIST_ADD_TAIL(header,list,node)				\
	if ( HEADER_PREVIOUS(header) != NULL )			\
		LIST_NEXT( list, HEADER_PREVIOUS(header) ) = node;	\
	else							\
		HEADER_NEXT(header) = node;			\
	LIST_NEXT(list,node) = NULL;					\
	LIST_PREVIOUS(list,node) = HEADER_PREVIOUS(header);		\
	HEADER_PREVIOUS(header) = node;


/*! Elimina un nodo de la lista.
 * ej:
 *		LIST_DEL(runqueue_hdr,runqueue,nodo55);
 */
#define LIST_DEL(header,list,node)						\
	if ( LIST_PREVIOUS(list,node) != NULL ) 				\
		LIST_NEXT( list, LIST_PREVIOUS(list,node) ) = LIST_NEXT(list,node);	\
	else								\
		HEADER_NEXT(header) = LIST_NEXT(list,node);			\
	if ( LIST_NEXT(list,node) != NULL )					\
		LIST_PREVIOUS( list, LIST_NEXT(list,node) ) = LIST_PREVIOUS(list,node);	\
	else								\
		HEADER_PREVIOUS(header) = LIST_PREVIOUS(list,node);
	

/*! Permite recorrer la lista de principio a fin.
 * Utiliza como argumentos un puntero del tipo de los nodos
 * y la lista.
 * ej:
 *		task_struct_t *nodotmp;
 *		LIST_FOREACH(nodotmp,runqueue_hdr,runqueue) {
 *			if ( nodotmp->priority > ... ) ..
 *			...
 *		}
 */
#define LIST_FOREACH(var,header,list)							\
	for(var=HEADER_NEXT(header); var!=NULL; var=LIST_NEXT(list,var))



/* \brief Simil LIST_FOREACH, pero se pueden remover los nodos que se recorren
 * su utilizacion es un poco compleja (deben agregarse dos l�neas de c�digo para su correcto uso)
 *
 *   _LIST_FOREACH(child, actual->childs, brothers) {
 *   	aux = LIST_NEXT(brothers, child);							// Agregado 1
 *   	if (child->estado == TASK_ZOMBIE)
 *   		pid = sys_waitpid(child->pid,&value,WNOHANG);
 *   	child = aux;												// Agregado 2
 *	 }
 * \todo Implementarlo m�s limpiamente
 */
#define _LIST_FOREACH(var,header,list)							\
	for(var=HEADER_NEXT(header); var!=NULL; )

/* \brief Permite insertar un nodo antes de otro nodo determinado
 * es importante que nodebefore sea un nodo v�lido, esto implica que
 * no se permite que nodebefore sea NULL por ejemplo.
 */
#define LIST_INSERT_BEFORE(header,list,nodebefore,node)				\
	LIST_PREVIOUS(list,node)=LIST_PREVIOUS(list,nodebefore);		\
	LIST_NEXT(list,node)=nodebefore;														\
	if ( HEADER_NEXT(header)==nodebefore )											\
		HEADER_NEXT(header)=node;																	\
	else																												\
		LIST_NEXT(list,LIST_PREVIOUS(list,nodebefore))=node;			\
	LIST_PREVIOUS(list,nodebefore)=node;

	


/*! Idem LIST_FOREACH pero desde el fin al ppio de la lista
 * ej:
 *    task_struct_t *nodotmp;
 *    LIST_FOREACH_REVERSE(nodotmp,runqueue_hdr,runqueue) {
 *       if ( nodotmp->priority > ... ) ..
 *       ...
 *    }
 */
#define LIST_FOREACH_REVERSE(var,header,list)						\
	for(var=HEADER_PREVIOUS(header); var!=NULL; var=LIST_PREVIOUS(list,var))


/* Permite buscar un nodo de la lista por un valor de
 * un atributo particular.
 * Recibe como argumentos la lista, la variable donde
 * devuelve un puntero al nodo encontrado o NULL en caso
 * de busqueda sin resultados, el siguiente argumento es
 * el nombre de la variable a buscar (definida en la 
 * estructura y por ultimo el valor buscado.
 * ej:
 *		task_struct_t *nodobuscado;
 *		LIST_FIND(runqueue,nodobuscado,priority,3)
 */
#define LIST_FIND(header,list,var,name,value)							\
	for(var=HEADER_NEXT(header); var!=NULL && var->name!=value; var=LIST_NEXT(list,var))


/*! \brief Pasa los nodos de una lista a otra
 *  \param dest header de la lista destino
 *  \param src header de la lista origen
 *  \param type tipo de dato que describe al nodo
 *  \note La lista origen queda totalmente deshecha
 */
#define LIST_ATTACH(dest, src, type)					\
{	type *aux;											\
	_LIST_FOREACH(ptr, src) {			\
		aux = LIST_NEXT(src,ptr);							\
		LIST_ADD_TAIL(dest,dest, ptr);	\
		ptr = aux;										\
	}													\
}

/*! \brief Vacia una lista, realiza esto reinicializando el header, no realiza ningun cambio
 * en los puntero de los nodos enlazados
 */
#define LIST_CLEAN(header)									\
	LIST_INIT(header);

#define LIST_REPLACE(list, original, replace)						\
	LIST_NEXT(list, replace) = LIST_NEXT(list, original);			\
	LIST_PREVIOUS(list, replace) = LIST_PREVIOUS(list, original);	\
	if (LIST_NEXT(list, replace))									\
		LIST_PREVIOUS(list,LIST_NEXT(list, replace)) = replace;		\
	if (LIST_PREVIOUS(list, replace))								\
		LIST_NEXT(list,LIST_PREVIOUS(list, replace)) = replace;

#endif
