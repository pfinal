/* 8259.h */

#ifndef __SYSTEM
#include "routix/system.h"
#endif

#define PIC1_A0	0x20
#define PIC1_A1	0x21
#define PIC2_A0	0xa0
#define PIC2_A1	0xa1


void enable_irq (byte irqn);
void disable_irq (byte irqn);

void inicializarPics(unsigned char int1, unsigned char int2);
void mascarasPics(unsigned char mask1, unsigned char mask2);



#define endOfInterrupt() outportb(PIC1_A0,0x20)
