/* event.h */


#include "task.h"
#include "device.h"


typedef struct event_t {
  device_t dispositivo;
  task_struct_t *proceso;
  struct event_t *proximo;
} event_t;
