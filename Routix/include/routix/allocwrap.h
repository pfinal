/*
 *	Wrapper para el manejo y control de alocamiento din�mico de memoria
 */

// 
// Comentar la siguiente linea para deshabilitar el wrapper de memoria
// 
#define __ALLOC_WRAPPER_ON


#ifndef __ALLOC_WRAPPER
#define __ALLOC_WRAPPER

// IMPORTANTE:
// El �ltimo componente de la enumeraci�n alloc_t debe ser MM_WRAPPER_UNKNOWN.
// Esto es ya que se utiliza como �ltimo elemento y adem�s se usa para capturar
// todas las llamadas a malloc y free que no fueron portadas al wrapper (se
// transforman en MALLOC(MM_WRAPPER_UNKNOWN,...)
typedef enum alloc_t { MM_WRAPPER_TIMER, MM_WRAPPER_FAT12 , MM_WRAPPER_USERPAGE, MM_TASK_SIGNALS, MM_WRAPPER_UNKNOWN } alloc_t;

#define MM_WRAPPER_MAX_ENTRIES	(MM_WRAPPER_UNKNOWN+1)


typedef struct mm_wrapper_t {
	int allocs;
	int frees;
} mm_wrapper_t;

#define ALLOCS(mm)	((mm).allocs)
#define FREES(mm)		((mm).frees)



// La variable __ALLOC_WRAPPER_ON esta definida en routix/allocwrap.h y se
// utiliza para habilitar y deshabilitar el wrapper (afecta tambien a kalloc.h)

#ifdef __ALLOC_WRAPPER_ON
// Wrapper activo	(debug)
#define MALLOC(alloc_type,size)		mm_wrapper_malloc((alloc_type),(size))
#define FREE(alloc_type,address)	mm_wrapper_free((alloc_type),(address))


#else
// Wrapper inactivo (m�xima performance)
#define MALLOC(alloc_type,size)		__malloc(size)
#define FREE(alloc_type,address)	__free(address)

#endif




// Funciones
void init_mm_wrapper(void);
inline void *mm_wrapper_malloc(alloc_t type, unsigned int bytes);
inline void mm_wrapper_free(alloc_t type, void *ptr);
void mm_wrapper_dump(alloc_t type);
void mm_wrapper_dump_all(void);


#endif
