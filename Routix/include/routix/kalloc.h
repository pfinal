/*! \brief Uso interno de malloc y free
 */

#include <routix/allocwrap.h>
#include <routix/system.h>


#ifndef __ALLOC_KERNEL
#define __ALLOC_KERNEL
 
#define PAGINA_SIZE 4096

void *malloc_page(void);
void *__malloc(unsigned nbytes);
void __free (void *ap);




// La variable __ALLOC_WRAPPER_ON esta definida en routix/allocwrap.h y se
// utiliza para habilitar y deshabilitar el wrapper
// Con las macros siguientes trapeamos todas las llamadas a malloc y free que
// no han sido portadas al wrapper (aparecer�n como tipo UNKNOWN).
//
#ifdef __ALLOC_WRAPPER_ON
// Wrapper activo	(debug)
#define malloc(nbytes)	(MALLOC(MM_WRAPPER_UNKNOWN,nbytes))
#define free(ap)				(FREE(MM_WRAPPER_UNKNOWN,ap))

#else
// Wrapper inactivo (m�xima performance)
#define malloc(nbytes)	__malloc(nbytes)
#define free(ap)				__free(ap)

#endif




typedef long Align;

typedef union header {
    struct {
	union header *ptr;
	unsigned size;
    } s;
    Align x;
} Header;

Header *morecore(void);


// Funciones
dword kmem_free (void);
addr_t get_free_page(void);
addr_t kmalloc_page(void);
int kfree_page(addr_t direccion);

struct user_page *umalloc_page ( word flags, addr_t vdir, addr_t cr3);
struct user_page *ufree_page (struct user_page *aux);
void inicializacion_kmalloc(int memoria_fisica, int memoria_kernel);

#endif
