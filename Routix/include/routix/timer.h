/* timer.h */

#include <routix/system.h>
#include <routix/task.h>


#ifndef __TIMER
#define __TIMER

extern dword jiffies;


// Habilitamos el modo seguro de remoci�n de timers donde se realiza
// su b�squeda previa antes de eliminarse
#define SECURE_TIMER_OPER


typedef struct timer_t {
	dword ticks;
  task_struct_t *process;
  void (*func)(struct timer_t *);			/// \var Puntero a la funcion a ejecutar

  void *data;						/// \var Puntero a datos genericos
  
	LIST_DATA(timer_t)	timer_list;			/// \var Entrada para poder pertenecer a un listado de timers
	
} timer_t;	

#define TIMER_TICKS(t)			((t)->ticks)
#define TIMER_PROCESS(t)		((t)->process)
#define TIMER_FUNCTION(t)		((t)->func)
#define TIMER_DATA(t)				((t)->data)
#define TIMER_STATE(t)			((t)->state)


void actualizar_timers(void);
inline timer_t *create_timer(dword ticks, task_struct_t *proceso, void (*func)(struct timer_t *info), void *data);
inline int clean_timer(timer_t *timer);

inline void timer_dump(void);


#define TIMER_USEGUNDOS(segundos) (segundos/10000)
#define USECONDS_TO_TICKS(useconds) ((useconds)/10000)

#endif
