/* thread.h */
#ifndef __THREAD
#define __THREAD

#include <routix/system.h>
#include <sys/list.h>
#include <routix/allocwrap.h>
#include <sys/bitmap.h>


//!	\brief	define el tama�o del stack en modo user de un thread
#define THREAD_STACK_SIZE				_1MB


/*!	\brief define la cantidad m�xima de threads por proceso
 *	El n�mero se encuentra limitado principalmente por el tama�o de stack que desee darse a cada thread.
 *	Como todos los threads comparten el mismo CR3 del proceso no pueden tener el stack de user en la misma address.
 *	El stack del thread principal crece en 0x90000000. Los dem�s deber�n estar desde ah� hasta 0xC0000000.
 */
#define MAX_THREADS_PER_PROCESS			(0xC0000000 - 0x90000000) / THREAD_STACK_SIZE



#endif	//__THREAD
