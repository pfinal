/*! \brief Definiciones b�sicas de entrada/salida para el kernel
 */

#include <sys/types.h>

int putchar ( int );
int puts ( const char *);
byte *nextpos ( byte *);
void clrscr (void);
void scroll_up ( void );
unsigned char *gotoxy ( int , int );
char getascii_v ( char );	// Getascii para video
char *gets(char *s);
void printn( unsigned int num, int base);
int atoi(const char *str);

unsigned char inportb (word puerto);
void outportb (word puerto, unsigned char dato);
unsigned char inportb_p (word puerto);
void outportb_p (word puerto, unsigned char dato);

inline int count_elements (char **vector);
