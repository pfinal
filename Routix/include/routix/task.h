/* task.h */

#ifndef __ROUTIX_TASK
#define __ROUTIX_TASK


#include <routix/system.h>
#include <routix/paging.h>
#include <sys/list.h>
#include <routix/allocwrap.h>
#include <routix/kalloc.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/bitmap.h>
#include <routix/thread.h>
#include <routix/file.h>
#include <routix/kalloc.h>


#define MAX_TASKS 7

typedef struct task_t {
	word previos_task_link;
	dword esp0;
	word ss0;
	dword esp1;
	word ss1;
	dword esp2;
	word ss2;
	dword cr3;
	dword eip;
	dword eflags;
	dword eax;
	dword ecx;
	dword edx;
	dword ebx;
	dword esp;
	dword ebp;
	dword esi;
	dword edi;
	word  es;
	word  cs;
	word  ss;
	word  ds;
	word  fs;
	word  gs;
	word  ldt;
	word  t;
	word  iomap;
} task_t;

//! TSS completo de la arquitectura IA32
typedef struct tss_t {
	word previos_task_link;
	word reservado0;
	dword esp0;
	word ss0;
	word reservado1;
	dword esp1;
	word ss1;
	word reservado2;
	dword esp2;
	word ss2;
	word reservado3;
	dword cr3;
	dword eip;
	dword eflags;
	dword eax;
	dword ecx;
	dword edx;
	dword ebx;
	dword esp;
	dword ebp;
	dword esi;
	dword edi;
	word  es;
	word  reservado4;
	word  cs;
	word  reservado5;
	word  ss;
	word  reservado6;
	word  ds;
	word  reservado7;
	word  fs;
	word  reservado8;
	word  gs;
	word  reservado9;
	word  ldt;
	word  reservado10;
	word  t;
	word  iomap;
} tss_t;

//! Esta estructura posee todos los registros que se "pop"ean del stack de modo kernel antes de ejecutar una tarea
struct int_regs_ext
{
    dword gs;
    dword fs;
    dword es;
    dword ds;
    dword eax;
    dword ebx;
    dword ecx;
    dword edx;
    dword edi;
    dword esi;
    dword ebp;
    dword eip;
    dword cs;
    dword eflags;
    dword esp;
    dword ss;
};


extern tss_t tss;
extern task_t tarea[];
void inicializarTss(tss_t *tss, word cs, word ds, dword eip, dword esp, dword eflags);

inline pid_t get_new_pid(void);
		
#define ltr(selector) __asm__ __volatile__("ltr %w0" : : "a" (selector));



#define MAX_PAGINAS_POR_TAREA	16
#define MAX_DESCRIPTION		64

//! Aqu� est�n contenida toda la informaci�n relacionada con handlers, m�scaras y se�ales pendientes
struct task_signals {
	struct sigaction signal[SIGMAX];
	unsigned char sigcount[SIGMAX];
	unsigned long sigpending;							//Se�ales pendientes
	sigset_t sigmask;
	addr_t sigcheck_addr;					//Aca guardo la dir del hardcode de la llamada a SYS_SIGNALS | SYS_SIGNAL_CHECK 
};

// Estas variables contienen la cantidad de alocaciones y liberacion de estructuras de se�ales
// (solo para control de leaks)
extern unsigned int task_signals_alloc, task_signals_free;

extern unsigned int umalloc_alloc, umalloc_free;

//! Retorna la direcci�n donde se ubica la estructura task_signals asociada con task
#define TASK_SIGNALS(task)				(task->_signals)
//! Retorna la m�scara de se�ales pendientes
#define TASK_SIGPENDING(task)			(TASK_SIGNALS(task)->sigpending)
//! Retorna la m�scara de se�ales inhibidas
#define TASK_SIGMASK(task)				(TASK_SIGNALS(task)->sigmask)
//! Retorna la ubicaci�n del wrapper que se ejecuta al finalizar un handler de se�al
#define TASK_SIGADDR(task)				(TASK_SIGNALS(task)->sigcheck_addr)
//! Permite el acceso a la estructura sigaction de la se�al signo en la tarea task
#define TASK_SIGNAL(task, signo)		(TASK_SIGNALS(task)->signal[signo])
//! Handler de la se�al signo
#define TASK_SIGNAL_HANDLER(task,signo)	(TASK_SIGNAL(task,signo).sa_handler)
//! M�scara de se�ales inhibidas mientras se ejecuta la se�al signo
#define TASK_SIGNAL_MASK(task,signo)	(TASK_SIGNAL(task,signo).sa_mask)
//! Flags de la se�al signo en la tarea task
#define TASK_SIGNAL_FLAGS(task,signo)	(TASK_SIGNAL(task,signo).sa_flags)
//! Aloca una estructura task_signals para la tarea tasr
#define TASK_SIGNALS_ALLOC(task)		TASK_SIGNALS(task) = (struct task_signals *)				\
															 MALLOC(MM_TASK_SIGNALS,sizeof(struct task_signals)); \
															 task_signals_alloc++;					
//! Libera la estructura task_signals asociada a la tarea task
#define TASK_SIGNALS_FREE(task)			FREE(MM_TASK_SIGNALS,TASK_SIGNALS(task)); \
										task_signals_free++;		

#define TASK_SIGNALS_INIT(task)			memset(TASK_SIGNALS(task), 0, sizeof(struct task_signals))
//! Cantidad de se�ales encoladas del tipo signo en la tarea task
#define TASK_SIGNAL_COUNT(task, signo) ((TASK_SIGNALS(task)->sigcount)[signo])
										
struct std_header {
	struct std_header *next;
	struct std_header *prev;
};									

//! Este es el task_struct, pilar fundamental de Routix
typedef struct task_struct_t
{
    dword esp0;
    dword cr3;
    dword cr3_backup;
    pid_t pid;
    struct task_struct_t *proxima;
    char descripcion[MAX_DESCRIPTION];
    byte estado;
    word prioridad;
    word cuenta;
    dword tiempo_cpu;
    struct file *open_files [MAX_FILES_POR_TAREA];  //definido en file.h
    word num_code, num_data, num_stack;	    //Cantidad de paginas de Codigo, Datos y stack
    int err_no;
    struct user_page *mcode;
    struct user_page *mdata;
    struct user_page *mstack;
	struct task_struct_t *padre;
    int retorno;
//    LIST_NEW(struct zombie_queue) zombie_header;
	LIST_DATA(task_struct_t) io_pending;				// Este proceso puede pertenecer a una lista
														// de entrada/salida, estando pendiente (mientras
														// se lee o escribe de disco por medio de disco)
														// ver "blockcache.c".
	LIST_NEW(struct task_struct_t) childs;				// Header de una lista con los hijosl proceso
	LIST_DATA(task_struct_t) brothers;					// Nodo de la lista que contiene a todos sus "bros"
						
	struct task_signals *_signals;
/*
	struct sigaction signals[SIGMAX];
	unsigned long sigpending;							//Se�ales pendientes
	addr_t sigcheck_addr;					//Aca guardo la dir del hardcode de la llamada a SYS_SIGNALS | SYS_SIGNAL_CHECK 
	sigset_t sigmask;
*/
	unsigned int thread_count;
	LIST_NEW(struct thread_struct_t) threads;			// Header de la lista enlazada que contiene a todos los threads
														// del proceso
	BITMAP(thread_bitmap, MAX_THREADS_PER_PROCESS);		// Bitmap indicador de threads del proceso


											
} task_struct_t ;

typedef struct thread_struct_t
{
    dword esp0;
    dword cr3;
    dword cr3_backup;
    pid_t tid;
    struct user_page *mstack;
    task_struct_t *task;					// Puntero al proceso que contiene al thread
    byte estado;
    word prioridad;
    word cuenta;
    dword tiempo_cpu;
	LIST_DATA(thread_struct_t) threads;		// Convierte a esta estructura en nodo de una lista
	
} thread_struct_t;


#define TASK_STACK_USER(t)			(t->esp0)
#define	TASK_CR3(t)					(t->cr3)
#define	TASK_CR3_BACKUP(t)			(t->cr3_backup)
#define	TASK_PID(t)					(t->pid)
#define	TASK_DESCRIPTION(t)			(t->descripcion)
#define	TASK_STATE(t)				(t->estado)
#define	TASK_PRIORITY(t)			(t->prioridad)
#define	TASK_COUNT(t)				(t->cuenta)
#define	TASK_CPUTIME(t)				(t->tiempo_cpu)
#define	TASK_CODEPAGES(t)			(t->num_code)
#define	TASK_DATAPAGES(t)			(t->num_data)
#define	TASK_STACKPAGES(t)			(t->num_stack)
#define	TASK_ERRNO(t)				(t->err_no)
#define	TASK_PPID(t)				(t->padre->pid)
#define TASK_PARENT(t)				(t->padre)

enum estado { TASK_RUNNING, TASK_STOPPED, TASK_INTERRUMPIBLE, TASK_ININTERRUMPIBLE, TASK_ZOMBIE, TASK_CLEAN };

extern task_struct_t *init_task, *pre_init_task;
inline void sleep_init();
inline void wakeup_init();

// Estructura que describe las paginas utilizadas por las tareas
#define PAGINA_DATA		0	    //pagina de datos
#define PAGINA_CODE		1	    //pagina de codigo
#define	PAGINA_STACK	0	    //pagina de stack	    
#define PAGINA_ALLOC	2	    //pagina alocada dinamicamente

struct user_page
{
    addr_t dir, vdir;
    word count;
    dword flags;
    struct user_page *next;
};




// Variables externas
extern task_struct_t *tareas_inicio;
extern task_struct_t *actual;

// Funciones para el manejo de tareas
task_struct_t *init_new_task(word cs, word ds, dword eip, dword esp, dword eflags, char *descripcion, word prioridad);
thread_struct_t * init_new_thread(word cs, word ds, dword eip, dword esp, dword eflags, char *descripcion, word prioridad);

task_struct_t *encontrar_proceso_por_pid(pid_t pid);
void tomar_nombre_tarea (const char *viejo, char *nuevo);
inline void dormir_task(task_struct_t *tarea);
inline void despertar_task(task_struct_t *tarea);
int insertar_tarea(task_struct_t *nueva);
int remover_task (task_struct_t *tarea);


// Macro para reschedulear, genera una llamada a la int 0x51 quien busca la pr�xima tarea a correr
#define _reschedule() __asm__ __volatile__("int $0x51")


//! Ubicaci�n l�gica del c�digo de una tarea
#define TASK_TEXT   0x80000000
//! Ubicaci�n l�gica de los datos de una tarea
#define TASK_DATA   0x88000000
//! Ubicaci�n l�gica del stack de modo usuario de una tarea
#define TASK_STACK  0x90000000

// Ubicacion logica de una pagina de kernel que poseera permisos de usuario para contener algunos wrappers 
// (como por ejemplo, una llamada a Syscall EXIT cuando termina el main de una tarea).
// EL pedido de la pagina, y la asignacion del c�digo de EXIT est� realizado en Kmain.c
#define EXIT_TASK	(TASK_TEXT - PAGINA_SIZE)

/// \def Prioridad inicial de un nuevo proceso creado por medio de exec
#define START_PRIORITY 1

// Cantidad de p�ginas del stack de una tarea
extern int TASK_STACK_SIZE;

//! Retorna el contexto de la tarea task cuando esta pasa a modo kernel v�a interrupci�n
#define GET_CONTEXT(task)	((struct int_regs_ext *) ((task->esp0 & 0xfffff000) + PAGINA_SIZE - sizeof(struct int_regs_ext)))

// Definiciones para debuggear memory leaks

extern int num_mallocs, num_frees;
extern int num_alloc_signals, num_free_signals;


// Estructura utilizada para almacenar informaci�n b�sica de un ejecutable
struct exec_file
{
	// Tama�o en bytes de cada segmento
	size_t code_len, data_len, bss_len;
	// Offset de la ubicaci�n de cada segmento en el archivo
	unsigned int code_off, data_off, bss_off;
	// Tama�o en p�ginas del segmento de c�digo, datos y datos+bss
	unsigned int code_pages, data_pages, alldata_pages;
};

//! Lee el header y obtiene toda la informaci�n base de un tipo COFF32 y lo coloca en *coff
int read_coff_header (int fd, struct exec_file *coff);


/*!	\brief	retorna la direccion de comienzo del proximo thread a crear
 *	\param	task_struct de la tarea
 *	\return	direccion de comienzo del stack de user del thread o NULL en caso contrario
 */
addr_t thread_get_stack_address(task_struct_t *task);

/*!	\brief	libera del bitmap el bit correspondiente a la address 
 *	\param	task_struct de la tarea
 *	\param	address direccion del stack
 *	\return	nada por ahora
 *	\note 	es utilizado cuando se libera un stack de un thread(por ej. cuando el thread finaliza)
 */
int thread_release_stack(task_struct_t *task, addr_t address);


#endif	// __ROUTIX_TASK
