/* signal.h */

#ifndef __SYSTEM
#include "system.h"
#endif

#ifndef __SIGNAL
#include <signal.h>
#endif

// Retorna 1 si una se�al est� inhibida o enmascarada
#define IS_MASKED(task,signo)	((TASK_SIGMASK(task) >> signo) & 1)


void exec_sigpending(task_struct_t *task, int signo, int first_signal);
int check_sigpending(task_struct_t *task, int save_context);
void sys_signal_correct(void);
void sys_signal_check (void);
int sys_sigprocmask(int flag, sigset_t *set, sigset_t *old_set);
int _sys_kill(task_struct_t *proceso, int signo);
int sys_sigaction (int sig, struct sigaction *act, struct sigaction *oact);
