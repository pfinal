/* system.h */


#ifndef __SYSTEM
#define __SYSTEM

#include "sys/types.h"

#define rdtscl(low) \
     __asm__ __volatile__ ("rdtsc" : "=A" (low))


/* Constantes de memoria f�sica y virtual */
#define KERNEL_FCODE	0
#define KERNEL_FDATA	0x100000
#define KERNEL_FSTACK_TOP 0x200000	
#define KERNEL_STACK_SIZE 0x20000
#define KERNEL_FSTACK	KERNEL_FSTACK_TOP - KERNEL_STACK_SIZE

#define KERNEL_END	KERNEL_FSTACK_TOP

#define KERNEL_CODE		0xc0000000
#define KERNEL_DATA		0xc8000000
#define KERNEL_STACK		0xd0000000
#define KERNEL_STACK_TOP	KERNEL_STACK + 0x8000000
#define KERNEL_INTERRUPT	KERNEL_STACK_TOP

#define _1MB			(1048576)

#define DESC_CODE		1<<3
#define DESC_DATA		2<<3
#define DESC_CODE_USUARIO	3<<3 | 3
#define DESC_DATA_USUARIO	4<<3 | 3
#define DESC_TSS0		5<<3
#define DESC_TSS1		6<<3

/* Variables Globales */
extern dword memoria;

extern addr_t KERNEL_PDT;
extern addr_t USER_PDT;

//Kernel PANIC (suceso inesperado o irrecuperable)
void kpanic(char *str);

#define nop() __asm__ __volatile__ ("nop")

#define cli() __asm__ __volatile__ ("pushf ; cli")
#define sti() __asm__ __volatile__ ("popf")

#define _cli()  __asm__ __volatile__ ("cli")
#define _sti()  __asm__ __volatile__ ("sti")

int getvar (char *);
int setvar (char *, int);
// Funciones de libreria
int sys_putc (char);
void kprintf ( char *string, ... );


#endif
