/* device.h
 * lista de dispositivos, por ahora hasta que tengamos
 * un manejo mas adecuado de devices
 */


#ifndef __DEVICE

// Lista de dispositivos
#define MAXDEVICEENTRIES	2
typedef enum device_t { TECLADO, fd0 } device_t;

typedef enum operation_t { IOREAD, IOWRITE } operation_t;

#define __DEVICE
#endif

