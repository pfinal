/*! \brief Definiciones exclusivas del kernel 
 *  \note no deben ni pueden ser usadas por la libreria Routix
 */

#ifndef __ROUTIX_TASK
	#include <routix/task.h>
#endif

void syscall (void);
int sys_no_existe (dword numero);

// Grupo Console
int sys_gets (char *str);
int sys_print (void *buff, size_t largo);
int sys_clrscr (void);

// Grupo process
int sys_fork (void);
int sys_void (void);
int sys_clone(void *func);
int sys_exec (char *nombre);
void sys_perror (char *str);
int sys_renice (word pid, word prioridad);
inline pid_t sys_get_pid (void);
inline pid_t sys_get_ppid (void);

void sys_exit (int);
int sys_exit_mm(task_struct_t *task);

void sys_show (int);
pid_t sys_wait(int *);
pid_t sys_waitpid (pid_t pid, int *status, int options);
#define WNOHANG 		1

// Grupo Signals

void *sys_signal (int signo, void (*func)() );
int sys_kill(pid_t pid, int signo);
void sys_signal_check(void);

// Grupo timer
int sys_sleep(int segundos);
int sys_usleep(int usegundos);
int sys_proc_dump(void);
//extern int sys_kill(pid_t pid, int sig);
int sys_proc_dump_v(int);
int sys_timer_dump(void);


// Grupo mem
void *sys_malloc_page(void);
int sys_free_page(void *);
dword sys_free_mem (void);
