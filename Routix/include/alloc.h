
#define PAGINA_SIZE 4096

void *malloc_page(void);

void *malloc(unsigned nbytes);
void free (void *ap);

typedef long Align;

typedef union header {
    struct {
	union header *ptr;
	unsigned size;
    } s;
    Align x;
} Header;

Header *morecore(void);


// Funciones
dword kmem_free (void);
addr_t get_free_page(void);
addr_t kmalloc_page(void);
int kfree_page(addr_t direccion);

struct user_page *umalloc_page ( word flags, addr_t vdir, addr_t cr3);
struct user_page *ufree_page (struct user_page *aux);
void inicializacion_kmalloc(int memoria_fisica, int memoria_kernel);

