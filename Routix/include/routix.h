/*! \brief header utilizado por la libreria Routix y las tareas de usuario
 */

#ifndef _ROUTIX
#define _ROUTIX

#ifndef __ROUTIX_TYPES
#include <sys/types.h>
#endif

/*
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned long dword;
typedef unsigned long long qword;

typedef int pid_t;

typedef dword addr_t;
typedef unsigned int size_t;
*/
//void gets (char *str);
void voido (void);
//int putchar (char car);
//void puts(char *str);
void printf ( char *string, ...);
int sprintf(char *str, const char *string, ...);
void sputchar (char car);
size_t strlen(const char *s);

pid_t getpid();
pid_t getppid();

int sleep(int);
int proc_dump(void);
#endif
